FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD /build/libs/*.jar app.jar
RUN adduser -D myuser
USER myuser
#EXPOSE 1433
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
CMD java $JAVA_OPTS -Dserver.port=$PORT -jar app.jar
