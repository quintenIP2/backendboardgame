package be.kdg.boardgame.dbseed

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.authentication.persistence.UserRepository
import be.kdg.boardgame.authentication.services.UserService
import be.kdg.boardgame.dbseed.letters.scores.LetterScoreInitializer
import be.kdg.boardgame.dbseed.words.WordInitializer
import be.kdg.boardgame.game._game.persistence.WordRepository
import be.kdg.boardgame.game._letter_rack.persistence.LetterRepository
import be.kdg.boardgame.game._statistics.model.Statistic
import be.kdg.boardgame.game._statistics.persistence.StatisticRepository
import org.apache.commons.io.FileUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component

@Component
class DbSeeder(bCryptPasswordEncoder: BCryptPasswordEncoder,
               userService: UserService) {
    @Autowired
    private lateinit var userRepository: UserRepository
    @Autowired
    private lateinit var letterRepository: LetterRepository
    @Autowired
    private lateinit var statisticRepository: StatisticRepository
    @Autowired
    private lateinit var wordRepository: WordRepository
    @Autowired
    private lateinit var letterScoreInitializers: List<LetterScoreInitializer>
    @Autowired
    private lateinit var wordInitializers: List<WordInitializer>

    private var friends: MutableSet<User> = mutableSetOf(
            User("nick@gmail.com", bCryptPasswordEncoder.encode("nick"), "nick",
                    accountIsActivated = true, avatar = userService.convertImageToString("/pictures/player-logo.png")),
            User("lotte@gmail.com", bCryptPasswordEncoder.encode("lotte"), "lotte",
                    accountIsActivated = true, avatar = userService.convertImageToString("/pictures/player-logo.png")),
            User("quinten@gmail.com", bCryptPasswordEncoder.encode("quinten"), "quinten",
                    accountIsActivated = true, avatar = userService.convertImageToString("/pictures/player-logo.png")),
            User("glenn@gmail.com", bCryptPasswordEncoder.encode("glenn"), "glenn",
                    accountIsActivated = true, avatar = userService.convertImageToString("/pictures/player-logo.png"))
    )

    private val hasFriends = User("jonathan@gmail.com", bCryptPasswordEncoder.encode("jonathan"),
            "jonathan", accountIsActivated = true, friends = friends,
            avatar = userService.convertImageToString("/pictures/player-logo.png"))

    @EventListener(ApplicationReadyEvent::class)
    fun run() {
        checkLetters()
        checkWords()

        val friendsArePresent = friends.map { f -> userRepository.findByEmail(f.email).isPresent }.reduce { acc, b -> acc && b }
        val hasFriendsIsPresent = userRepository.findByEmail(hasFriends.email).isPresent
        if (!friendsArePresent) initialiseUsers()
        if (!hasFriendsIsPresent) initialiseHasFriends()

        val users = userRepository.findAll()
        users.forEach {
            val statistic = statisticRepository.findByPlayer(it)
            statistic ?: statisticRepository.save(Statistic(it))
        }

        //        val botIsPresent = userRepository.findByEmail(bot.email).isPresent
        //        if (!botIsPresent) initialiseBot()

    }

    private fun checkLetters(): Boolean {
        var present = true
        for (init in letterScoreInitializers) {
            val letters = init.initLetterScores()
            val lettersInDb = letterRepository.findAllByLanguage(letters.first().language)

            present = lettersInDb.size == letters.size

            if (!present) {
                letterRepository.deleteAll(lettersInDb)
                letterRepository.saveAll(letters)
            }
        }

        println("all letters are present: $present")
        return present
    }

    private fun checkWords(): Boolean {
        var present = true
        for (init in wordInitializers) {
            val words = init.initWords()
            val wordsInDb = wordRepository.findAllByLanguage(words.first().language)

            present = wordsInDb.size == words.size

            if (!present) {
                wordRepository.deleteAll(wordsInDb)
                wordRepository.saveAll(words)
            }
        }

        println("all data is present: $present")
        return !present
    }

    // some test accounts to get up and running
    private fun initialiseUsers() {
        userRepository.saveAll(friends)
    }

    private fun initialiseHasFriends() {
        userRepository.save(hasFriends)
    }

//    private fun initialiseBot() {
//        userRepository.save(bot)
//    }
}
