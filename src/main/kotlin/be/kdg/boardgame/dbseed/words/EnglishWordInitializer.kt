package be.kdg.boardgame.dbseed.words

import be.kdg.boardgame.game._game.model.Word
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class EnglishWordInitializer
    : WordInitializer {
    @Autowired
    private lateinit var loader: WordLoader

    private val language = "en_US"

    override fun initWords(): Collection<Word> {
        return loader.loadWords(language).map {
            Word(it, language)
        }
    }
}
