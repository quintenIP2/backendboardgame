package be.kdg.boardgame.dbseed.words

interface WordLoader {
    /**
     * Load [Word]s from a source.
     * @param language Language to be loaded.
     * @return List of loaded words.
     * @author Nick Van Osta
     */
    fun loadWords(language: String): Collection<String>
}
