package be.kdg.boardgame.dbseed.words

import org.springframework.stereotype.Component
import java.io.InputStreamReader

@Component
class FileWordLoader
    : WordLoader {
    private val fileLocation = "dictionaries"
    private val fileExtension = "dict"

    override fun loadWords(language: String): Collection<String> {
        val classLoader: ClassLoader = javaClass.classLoader
        val words = mutableListOf<String>()
        val filename = "$fileLocation/$language.$fileExtension"

        with(InputStreamReader(classLoader.getResourceAsStream(filename))) {
            words.addAll(this.readLines().map { it.toUpperCase() })
        }

        return words
    }
}
