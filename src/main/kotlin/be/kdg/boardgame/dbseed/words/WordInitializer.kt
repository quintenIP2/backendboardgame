package be.kdg.boardgame.dbseed.words

import be.kdg.boardgame.game._game.model.Word

interface WordInitializer {
    /**
     * Initialize [Word]s.
     * @return List of [Word]s.
     * @author Nick Van Osta
     */
    fun initWords(): Collection<Word>
}
