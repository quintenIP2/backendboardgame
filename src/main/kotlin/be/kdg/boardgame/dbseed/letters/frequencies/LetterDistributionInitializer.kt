package be.kdg.boardgame.dbseed.letters.frequencies

import be.kdg.boardgame.game._letter_rack.models.Letter

interface LetterDistributionInitializer {
    /**
     * Initialize [Letter]s with their frequencies.
     * @return List of [Letter]s.
     * @author Nick Van Osta
     */
    fun initLetterFrequencies(): Collection<Letter>
}
