package be.kdg.boardgame.dbseed.letters.frequencies

import be.kdg.boardgame.game._letter_rack.models.Letter
import be.kdg.boardgame.game._letter_rack.services.LetterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class EnglishLetterDistributionInitializer
    : LetterDistributionInitializer {
    @Autowired
    private lateinit var letterService: LetterService

    private val language = "en_US"
    private val letters = mapOf(
            'A' to 9,
            'B' to 2,
            'C' to 2,
            'D' to 4,
            'E' to 12,
            'F' to 2,
            'G' to 3,
            'H' to 2,
            'I' to 9,
            'J' to 1,
            'K' to 1,
            'L' to 4,
            'M' to 2,
            'N' to 6,
            'O' to 8,
            'P' to 2,
            'Q' to 1,
            'R' to 6,
            'S' to 4,
            'T' to 6,
            'U' to 4,
            'V' to 2,
            'W' to 2,
            'X' to 1,
            'Y' to 2,
            'Z' to 1,
            '.' to 2
    )

    override fun initLetterFrequencies(): Collection<Letter> {
        val list = mutableListOf<Letter>()

        letters.forEach { (k, v) ->
            val letter = letterService.getLetterByCharAndLanguage(k, language)
            for (i in 0 until v) {
                list.add(letter)
            }
        }

        return list
    }
}
