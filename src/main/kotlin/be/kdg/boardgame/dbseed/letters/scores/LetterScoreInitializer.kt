package be.kdg.boardgame.dbseed.letters.scores

import be.kdg.boardgame.game._letter_rack.models.Letter

interface LetterScoreInitializer {
    /**
     * Initialize [Letter] scores.
     * @return List of [Letter].
     * @author Nick Van Osta
     */
    fun initLetterScores(): Collection<Letter>
}
