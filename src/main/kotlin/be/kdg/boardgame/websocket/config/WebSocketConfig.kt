package be.kdg.boardgame.websocket.config

import org.springframework.context.annotation.Configuration
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer

@Configuration
@EnableWebSocketMessageBroker
class WebSocketConfig : WebSocketMessageBrokerConfigurer {


    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        registry.addEndpoint("/connect")
                .setAllowedOrigins("*")
                .withSockJS()
    }

    /**
     * destination prefixes are the endpoints for topics where the data will be broadcasted on
     * and where clients can subscribe on
     */
    override fun configureMessageBroker(registry: MessageBrokerRegistry) {
        registry.enableSimpleBroker(
                "/games/receive/",
                "/games/start/receive/",
                "/games/new/receive/",
                "/games/end/receive/",
                "/chat/receive/",
                "/notifications/receive/"
        )
    }
}
