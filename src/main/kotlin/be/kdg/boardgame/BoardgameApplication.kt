package be.kdg.boardgame

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan


@SpringBootApplication
@ComponentScan(lazyInit = true)
class BoardgameApplication

fun main(args: Array<String>) {
    runApplication<BoardgameApplication>(*args)
}

