package be.kdg.boardgame.mapper

import org.modelmapper.ModelMapper
import org.springframework.context.annotation.Configuration

/** made it autowirable */
@Configuration
class ModelMapperImpl : ModelMapper()
