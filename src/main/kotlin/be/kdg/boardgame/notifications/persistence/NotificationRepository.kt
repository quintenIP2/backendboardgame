package be.kdg.boardgame.notifications.persistence

import be.kdg.boardgame.notifications.models.Notification
import org.springframework.data.jpa.repository.JpaRepository


interface NotificationRepository : JpaRepository<Notification, String> {
    fun findAllByUserEmail(email: String): MutableList<Notification>
}
