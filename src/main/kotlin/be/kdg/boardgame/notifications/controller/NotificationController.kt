package be.kdg.boardgame.notifications.controller

import be.kdg.boardgame.notifications.dto.NotificationDTO
import be.kdg.boardgame.notifications.service.NotificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/")
class NotificationController {

    @Autowired
    lateinit var notificationService: NotificationService

    /**
     * fetches the history of notifications belonging to a certain username, of the last 10min
     */
    @GetMapping("/notifications")
    fun getAllNotificationsForUser(authentication: Authentication): ResponseEntity<List<NotificationDTO>> {

        if (authentication.isAuthenticated) {
            val notifs = notificationService
                    .getAllNotifsByEmail(authentication.name)
                    .map { n -> NotificationDTO(n.id, n.content, n.title, n.gameId, n.date) }
            notifs.forEach { println(it) }
            return ResponseEntity.ok(notifs)
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null)
    }

    /**
     * fetches the history of notifications belonging to a certain username, of the last 10min
     */
    @DeleteMapping("/notifications/{notifId}")
    fun deleteNotification(authentication: Authentication,
                           @PathVariable notifId: String): ResponseEntity<Boolean> {

        if (authentication.isAuthenticated) {
            val result = notificationService.deleteNotificationById(notifId)
            return ResponseEntity.ok(result)
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null)
    }

}
