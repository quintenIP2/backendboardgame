package be.kdg.boardgame.notifications.models

import be.kdg.boardgame.authentication.models.User
import org.hibernate.annotations.GenericGenerator
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "tbl_notification")
data class Notification(
        @Column
        val content: String = "",
        @Column
        val title: String = "",
        @Column
        val gameId: String = "",
        @Column
        val date: LocalDateTime = LocalDateTime.now(),
        @ManyToOne
        val user: User
) {
    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    var id: String = ""

}
