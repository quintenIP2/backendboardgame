package be.kdg.boardgame.notifications.service

import be.kdg.boardgame.authentication.services.UserService
import be.kdg.boardgame.notifications.dto.NotificationDTO
import be.kdg.boardgame.notifications.models.Notification
import be.kdg.boardgame.notifications.persistence.NotificationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service

@Service
class NotificationServiceImpl : NotificationService {

    @Autowired
    lateinit var notificationRepository: NotificationRepository
    @Autowired
    lateinit var userService: UserService
    @Autowired
    private lateinit var template: SimpMessagingTemplate


    override fun getAllNotifsByEmail(email: String): MutableList<Notification> {
        return notificationRepository.findAllByUserEmail(email)
    }

    /**
     * Creates a notification that sends a message about a certain topic to 1 or more users
     */
    override fun createNotification(destination: MutableList<String>, gameId: String, topic: String, message: String, excludeUsers: MutableList<String>) {
        destination.map { username ->
            if (!excludeUsers.contains(username)) {
                val user = userService.findByUsername(username)
                val n = Notification(message, topic, gameId, user = user)
                save(n)
                println("sending ${n.title} to ${user.username}")

                this.template.convertAndSend(
                        "/notifications/receive/$username", NotificationDTO(n.id, n.content, n.title, gameId)
                )
            }
        }
    }

    override fun deleteNotificationById(id: String): Boolean {
        notificationRepository.deleteById(id)
        return !notificationRepository.findById(id).isPresent
    }

    override fun save(n: Notification): Notification {
        return notificationRepository.save(n)
    }
}
