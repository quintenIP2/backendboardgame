package be.kdg.boardgame.notifications.service

import be.kdg.boardgame.notifications.models.Notification


interface NotificationService {
    fun getAllNotifsByEmail(email: String): MutableList<Notification>
    fun save(n: Notification): Notification
    fun createNotification(destination: MutableList<String>, gameId: String, topic: String, message: String, excludeUsers: MutableList<String>)
    fun deleteNotificationById(id: String): Boolean
}
