package be.kdg.boardgame.notifications.dto

import java.time.LocalDateTime

data class NotificationDTO(val id: String = "",
                           val content: String = "",
                           val notifTitle: String = "",
                           val gameId: String = "",
                           val date: LocalDateTime = LocalDateTime.now())
