package be.kdg.boardgame.game._overview.controller

import be.kdg.boardgame.authentication.dto.UserDTO
import be.kdg.boardgame.game._game.service.GameService
import be.kdg.boardgame.game._game.service.TurnService
import be.kdg.boardgame.game._overview.services.OverviewService
import be.kdg.boardgame.game._overview.dto.GameOverviewDTO
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/")
class OverviewController(val overviewService: OverviewService,
                         val gameService: GameService,
                         val turnService: TurnService) {

    //  CRUD
    @GetMapping("overview")
    fun getAllGames(authentication: Authentication): List<GameOverviewDTO> {
        if (authentication.isAuthenticated) {
            return overviewService.getGameOverviewForPlayer(authentication.name).map {
                GameOverviewDTO(
                        it.id,
                        it.title,
                        it.currentlyPlaying,
                        it.users.map { player ->
                            UserDTO(player.username, player.email, player.avatar)
                        },
                        UserDTO(it.creator.username, it.creator.email, it.creator.avatar),
                        it.isOver,
                        it.hasStarted,
                        it.winner,
                        turnService.getAllTurnsByGameId(it.id).size
                )
            }
        }
        return emptyList()
    }

    //  CRUD
    @GetMapping("overview/accepted")
    fun getAllAcceptedGames(authentication: Authentication): List<GameOverviewDTO> {
        if (authentication.isAuthenticated) {
            return overviewService.getAcceptedOverviewGamesForPlayer(authentication.name).map {
                GameOverviewDTO(
                        it.id,
                        it.title,
                        it.currentlyPlaying,
                        it.users.map { player ->
                            UserDTO(player.username, player.email, player.avatar)
                        },
                        UserDTO(it.creator.username, it.creator.email, it.creator.avatar),
                        it.isOver,
                        it.hasStarted,
                        it.winner,
                        turnService.getAllTurnsByGameId(it.id).size
                )
            }
        }
        return emptyList()
    }


}
