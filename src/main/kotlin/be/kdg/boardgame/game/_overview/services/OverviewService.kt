package be.kdg.boardgame.game._overview.services

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.authentication.persistence.UserRepository
import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._game.persistence.GameRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class OverviewService(
        val userRepository: UserRepository,
        val gameRepository: GameRepository
) {
    fun getGameOverviewForPlayer(email: String): List<Game> {
        val opt: Optional<User> = userRepository.findByEmail(email)
        if (opt.isPresent) {
            return gameRepository.findAllByUsersContains(opt.get())
        }
        return emptyList()
    }

    fun getAcceptedOverviewGamesForPlayer(email: String): List<Game> {
        val opt: Optional<User> = userRepository.findByEmail(email)
        if (opt.isPresent) {
            var games = gameRepository.findAllByUsersContains(opt.get())
            games = games.map { it.invitations.filter { i -> i.hasAccepted && i.user.email == email }.map { i -> i.game } }.flatten()
            println("games that have been accepted by user $email: ")
            games.forEach { println("game: ${it.id} | ${it.title} | ${it.hasStarted}") }
            return games
        }
        return emptyList()
    }
}
