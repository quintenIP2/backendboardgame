package be.kdg.boardgame.game._overview.dto

import be.kdg.boardgame.authentication.dto.UserDTO

data class GameOverviewDTO(
        val id: String,
        val title: String,
        val currentlyPlaying: Int,
        val players: List<UserDTO>,
        val creator: UserDTO,
        val over: Boolean,
        val hasStarted: Boolean,
        val winner: String,
        val turns: Int
)


