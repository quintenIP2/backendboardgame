package be.kdg.boardgame.game._settings.model.models

import be.kdg.boardgame.authentication.models.User
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

/**
 * The language and privacy _settings linked to a [User].
 *
 * @property player the [User] linked to these _settings.
 * @property language the language setting for the app's UI.
 * @property receiveMobileNotifications the privacy setting that controls wether or not the [User] receives mobile push _notifications.
 * @property receiveMailNotification the privacy setting that controls wether or not the [User] receives email _notifications.
 * @property isPublicStatistic the privacy setting that controls wether other [User]s can see the linked [User]'s Statistics.
 * @constructor Creates new _settings for a given [User].
 * @author Nick Van Osta & Quinten Spillemaeckers
 */
@Entity
@Table(name = "tbl_settings")
data class  PlayerSettings(
        @OneToOne
        var player: User,
        var language: String = "nl-BE",
        var receiveMobileNotifications: Boolean = true,
        var receiveMailNotification: Boolean = true,
        var isPublicStatistic: Boolean = true
) {
    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    var id: String? = null
}
