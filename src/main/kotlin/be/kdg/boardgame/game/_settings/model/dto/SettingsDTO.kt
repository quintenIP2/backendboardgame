package be.kdg.boardgame.game._settings.model.dto

import be.kdg.boardgame.authentication.models.User

data class SettingsDTO (val player: User,
                        val language: String,
                        val receiveMobileNotifications: Boolean,
                        val receiveMailNotification: Boolean,
                        val isPublicStatistic: Boolean)