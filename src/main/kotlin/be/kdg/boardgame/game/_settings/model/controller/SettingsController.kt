package be.kdg.boardgame.game._settings.model.controller

import be.kdg.boardgame.authentication.persistence.UserRepository
import be.kdg.boardgame.game._settings.model.dto.SettingsDTO
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("api/")
class SettingsController(val userRepository: UserRepository) {

    @GetMapping("settings")
    fun getSettings(auth: Authentication): ResponseEntity<SettingsDTO> {
        val player = userRepository.findByEmail(auth.name)
        return ResponseEntity.ok(SettingsDTO(
                player.get(),
                player.get().settings!!.language,
                player.get().settings!!.receiveMobileNotifications,
                player.get().settings!!.receiveMailNotification,
                player.get().settings!!.isPublicStatistic
        ))
    }
}