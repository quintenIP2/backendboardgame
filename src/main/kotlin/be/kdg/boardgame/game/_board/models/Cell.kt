package be.kdg.boardgame.game._board.models

import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

/**
 * One cell on the playing _board.
 *
 * @property multiplier the score multiplier of the cell.
 * @property tile the [Tile] placed on this cell, null if there is none.
 * @constructor Creates a cell with a given multiplier and tile if given.
 * @author Nick Van Osta & Quinten Spillemaeckers
 */
@Entity
@Table(name = "tbl_cell")
data class Cell(
        @ManyToOne(cascade = [CascadeType.ALL])
        val board: Board? = null,
        @Column
        val x: Int,
        @Column
        val y: Int,
        @Column
        val multiplier: Multiplier,

        @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
        var tile: Tile? = null,

        @Column
        var used: Boolean = false,

        @Id
        @Column
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "uuid2")
        var id: String? = null
)
