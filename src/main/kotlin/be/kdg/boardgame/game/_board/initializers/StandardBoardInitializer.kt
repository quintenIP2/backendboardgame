package be.kdg.boardgame.game._board.initializers

import be.kdg.boardgame.game._board.models.Board
import be.kdg.boardgame.game._board.models.Cell
import be.kdg.boardgame.game._board.models.Multiplier
import be.kdg.boardgame.game._game.model.Game
import org.springframework.stereotype.Component

@Component
class StandardBoardInitializer
    : BoardInitializer {
    private val h = 15
    private val w = 15

    private val multiplierGrid = arrayOf(
            3, 0, 0, -2, 0, 0, 0, 3, 0, 0, 0, -2, 0, 0, 3,
            0, 2, 0, 0, 0, -3, 0, 0, 0, -3, 0, 0, 0, 2, 0,
            0, 0, 2, 0, 0, 0, -2, 0, -2, 0, 0, 0, 2, 0, 0,
            -2, 0, 0, 0, 2, 0, 0, -2, 0, 0, 2, 0, 0, 0, -2,
            0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0,
            0, -3, 0, 0, 0, -3, 0, 0, 0, -3, 0, 0, 0, -3, 0,
            0, 0, -2, 0, 0, 0, -2, 0, -2, 0, 0, 0, -2, 0, 0,
            3, 0, 0, -2, 0, 0, 0, 2, 0, 0, 0, -2, 0, 0, 3,
            0, 0, -2, 0, 0, 0, -2, 0, -2, 0, 0, 0, -2, 0, 0,
            0, -3, 0, 0, 0, -3, 0, 0, 0, -3, 0, 0, 0, -3, 0,
            0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0,
            -2, 0, 0, 0, 2, 0, 0, -2, 0, 0, 2, 0, 0, 0, -2,
            0, 0, 2, 0, 0, 0, -2, 0, -2, 0, 0, 0, 2, 0, 0,
            0, 2, 0, 0, 0, -3, 0, 0, 0, -3, 0, 0, 0, 2, 0,
            3, 0, 0, -2, 0, 0, 0, 3, 0, 0, 0, -2, 0, 0, 3
    )

    override fun initBoard(game: Game): Board {
        val board = Board(game, h, w)
        val cells: List<Cell> = List(h * w) {
            var multiplier: Multiplier = Multiplier.NONE
            when (multiplierGrid[it]) {
                2 -> multiplier = Multiplier.X2_WORD
                -2 -> multiplier = Multiplier.X2_LETTER
                3 -> multiplier = Multiplier.X3_WORD
                -3 -> multiplier = Multiplier.X3_LETTER
            }

            Cell(board, it % w, it / w, multiplier, null)
        }

        board.cells.addAll(cells)
        return board
    }
}
