package be.kdg.boardgame.game._board.service

import be.kdg.boardgame.game._board.models.Cell

interface CellService {
    /**
     * Get [Cell] by its id.
     * @param id [Cell]'s id.
     * @return The requested [Cell].
     * @author Nick Van Osta
     */
    fun getCellById(id: String): Cell

    /**
     * Get [Cell] by its coordinates for a [Game].
     * @param email [User]'s email address.
     * @param gameName [Game]'s title.
     * @param x x coordinate.
     * @param y y coordinate.
     * @return The requested [Cell].
     * @author Nick Van Osta
     */
    fun getCellByCoordinates(email: String, gameName: String, x: Int, y: Int): Cell
}
