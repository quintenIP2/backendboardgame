package be.kdg.boardgame.game._board.service

import be.kdg.boardgame.game._board.models.Board
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._game.model.Game
import org.springframework.stereotype.Service

@Service
interface BoardService {
    /**
     * Get [Board] by its id.
     * @param id [Board]'s id.
     * @return The requested [Board].
     * @author Jonathan Peers & Nick Van Osta
     */
    fun getBoard(id: String): Board

    /**
     * Generate a new [Board] for a [Game].
     * @param game [Game] that needs its [Board] generated.
     * @return The generated [Board].
     * @author Nick Van Osta
     */
    fun generateBoard(game: Game): Board

    /**
     * Place a [Tile] on the [Board] on a given coordinate.
     * @param boardId [Board]'s id.
     * @param tile [Tile] to place down.
     * @param x x coordinate.
     * @param y y coordinate.
     * @author Jonathan Peers & Nick Van Osta
     */
    fun layTile(boardId: String, tile: Tile, x: Int, y: Int)

    /**
     * Remove the [Tile] on a given coordinate from the [Board].
     * @param boardId: [Board]'s id.
     * @param x x coordinate.
     * @param y y coordinate.
     * @author Jonathan Peers & Nick Van Osta
     */
    fun removeTile(boardId: String, x: Int, y: Int)
}
