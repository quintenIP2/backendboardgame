package be.kdg.boardgame.game._board.persistence

import be.kdg.boardgame.game._board.models.Board
import org.springframework.data.jpa.repository.JpaRepository

interface BoardRepository : JpaRepository<Board, String>
