package be.kdg.boardgame.game._board.service

import be.kdg.boardgame.game._board.exceptions.IllegalMoveException
import be.kdg.boardgame.game._board.initializers.BoardInitializer
import be.kdg.boardgame.game._board.models.Board
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._board.persistence.BoardRepository
import be.kdg.boardgame.game._game.model.Game
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BoardServiceImpl
    : BoardService {
    @Autowired
    private lateinit var boardRepository: BoardRepository
    @Autowired
    private lateinit var boardInitializer: BoardInitializer

    @Throws(Exception::class)
    override fun generateBoard(game: Game): Board {
        val shit = boardInitializer.initBoard(game)
        return boardRepository.save(shit)
    }

    override fun getBoard(id: String): Board {
        val optionalBoard = boardRepository.findById(id)
        if (optionalBoard.isPresent) {
            return optionalBoard.get()
        }
        throw Exception("Board not found!")
    }

    override fun layTile(boardId: String, tile: Tile, x: Int, y: Int) {
        val board = getBoard(boardId)
        val index = y * board.width + x
        val cell = board.cells[index]

        if (cell.tile != null) throw IllegalMoveException("The tile cannot be placed because there is already a tile on 'x: $x, y:$y' with letter '${cell.tile?.letter?.char}'")

        tile.cell = cell
        cell.tile = tile

        board.cells[index] = cell
        boardRepository.save(board)
    }

    override fun removeTile(boardId: String, x: Int, y: Int) {
        val board = getBoard(boardId)
        val index = y * board.width + x
        val cell = board.cells[index]

        cell.tile?.cell = null
        cell.tile = null

        board.cells[index] = cell
        boardRepository.save(board)
    }
}
