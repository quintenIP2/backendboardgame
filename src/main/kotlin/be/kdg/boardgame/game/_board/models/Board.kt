package be.kdg.boardgame.game._board.models

import be.kdg.boardgame.game._game.model.Game
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

/**
 * The Scrabble playing _board.
 * [cells] places to put a tile/letter
 * 
 * @author Nick Van Osta & Quinten Spillemaeckers & Jonathan Peers
 */
@Entity
@Table(name = "tbl_board")
data class Board(
        @OneToOne(cascade = [CascadeType.ALL])
        val game: Game,
        @Column
        var width: Int = 15,
        @Column
        var height: Int = 15,
        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
        var cells: MutableList<Cell> = mutableListOf()
) {
    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    var id: String? = null

    override fun toString(): String {
        return cells.map { it.tile?.letter?.char ?: '_' }.joinToString(separator = "") { it.toString() }
    }
}




