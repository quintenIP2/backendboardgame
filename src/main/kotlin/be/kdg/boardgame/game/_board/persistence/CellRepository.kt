package be.kdg.boardgame.game._board.persistence

import be.kdg.boardgame.game._board.models.Board
import be.kdg.boardgame.game._board.models.Cell
import org.springframework.data.jpa.repository.JpaRepository

interface CellRepository : JpaRepository<Cell, String> {
    fun findByBoardAndXAndY(board: Board, x: Int, y: Int) : Cell?
}
