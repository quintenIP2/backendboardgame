package be.kdg.boardgame.game._board.exceptions

import java.lang.RuntimeException

class CellNotFoundException(
        message: String? = "",
        cause: Throwable? = null
)
    : RuntimeException(message, cause)
