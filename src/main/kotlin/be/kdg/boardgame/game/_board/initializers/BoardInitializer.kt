package be.kdg.boardgame.game._board.initializers

import be.kdg.boardgame.game._board.models.Board
import be.kdg.boardgame.game._game.model.Game

interface BoardInitializer {
    fun initBoard(game: Game): Board
}
