package be.kdg.boardgame.game._board.models

/**
 * Score multiplier for [Cell]s on the [Board].
 *
 * @author Nick Van Osta & Quinten Spillemaeckers
 */
enum class Multiplier(val value: Int) {
    NONE(0),
    X2_LETTER(-2),
    X3_LETTER(-3),
    X2_WORD(2),
    X3_WORD(3)
}
