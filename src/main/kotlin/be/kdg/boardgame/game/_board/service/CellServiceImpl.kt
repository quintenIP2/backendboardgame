package be.kdg.boardgame.game._board.service

import be.kdg.boardgame.game._board.exceptions.CellNotFoundException
import be.kdg.boardgame.game._board.models.Cell
import be.kdg.boardgame.game._board.persistence.CellRepository
import be.kdg.boardgame.game._game.exceptions.GameNotFoundException
import be.kdg.boardgame.game._game.service.GameService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CellServiceImpl
    : CellService{
    @Autowired
    private lateinit var cellRepository: CellRepository
    @Autowired
    private lateinit var gameService: GameService

    override fun getCellById(id: String): Cell {
        return cellRepository.findById(id).orElseThrow { CellNotFoundException("No cell found with id '$id'") }
    }

    override fun getCellByCoordinates(email: String, gameName: String, x: Int, y: Int): Cell {
        val board = gameService.getGameByName(email, gameName).board ?: throw GameNotFoundException("No board found for game '$gameName'")
        return cellRepository.findByBoardAndXAndY(board, x, y) ?: throw CellNotFoundException("No")
    }
}
