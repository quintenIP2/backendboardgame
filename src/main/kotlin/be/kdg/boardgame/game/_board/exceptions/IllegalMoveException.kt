package be.kdg.boardgame.game._board.exceptions

import java.lang.RuntimeException

class IllegalMoveException(
        message: String? = "",
        cause: Throwable? = null
)
    : RuntimeException(message, cause)
