package be.kdg.boardgame.game._board.models

import be.kdg.boardgame.game._letter_rack.models.Letter
import be.kdg.boardgame.game._letter_rack.models.LetterRack
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

/**
 * A letter tile that can be placed on the [Board], be in someone's [LetterRack], or neither.
 *
 * @property letter the [Letter] on this tile.
 * @property letterRack the [LetterRack] this tile is on, null if not on a [LetterRack].
 * @property cell the [Cell] this tile is placed on, null if not on a [Cell].
 * @constructor Creates a new tile with a given [Letter] on it.
 * @author Nick Van Osta & Quinten Spillemaeckers
 */

@Entity
@Table(name = "tbl_tile")
data class Tile(
        @ManyToOne(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
        val letter: Letter,
        @ManyToOne(cascade = [CascadeType.ALL])
        val letterRack: LetterRack? = null,
        @OneToOne(cascade = [CascadeType.ALL])
        var cell: Cell? = null
){
    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    var id: String? = null
}
