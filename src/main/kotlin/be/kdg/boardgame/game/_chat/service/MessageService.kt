package be.kdg.boardgame.game._chat.service

import be.kdg.boardgame.game._chat.models.ChatMessage

interface MessageService {
    fun getChatMessagesByGame(gameId: String): MutableList<ChatMessage>
    fun save(gameId: String, content: String, name: String): ChatMessage
}
