package be.kdg.boardgame.game._chat.service

import be.kdg.boardgame.game._chat.models.ChatMessage
import be.kdg.boardgame.game._chat.persistence.ChatMessageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MessageServiceImpl : MessageService {
    @Autowired
    lateinit var chatMessageRepo: ChatMessageRepository

    override fun save(gameId: String, content: String, name: String): ChatMessage {
        return chatMessageRepo.save(ChatMessage(content, gameId, name))
    }

    override fun getChatMessagesByGame(gameId: String): MutableList<ChatMessage> {
        return chatMessageRepo.findAllByGameId(gameId)
    }
}
