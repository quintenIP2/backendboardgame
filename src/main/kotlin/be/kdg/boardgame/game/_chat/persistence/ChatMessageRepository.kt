package be.kdg.boardgame.game._chat.persistence

import be.kdg.boardgame.game._chat.models.ChatMessage
import org.springframework.data.jpa.repository.JpaRepository

interface ChatMessageRepository : JpaRepository<ChatMessage, String> {
    fun findAllByGameId(id: String): MutableList<ChatMessage>
}
