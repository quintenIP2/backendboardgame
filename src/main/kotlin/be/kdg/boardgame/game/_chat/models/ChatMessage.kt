package be.kdg.boardgame.game._chat.models

import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

/**
 * A _chat message sent by a player to a _chat room.
 *
 * @property userName the user's name who sent the message.
 * @property text the message of the message.
 * @constructor Creates a new message sent by a User.
 * @author Nick Van Osta & Quinten Spillemaeckers
 */
@Entity
@Table(name = "tbl_chatmessage")
data class ChatMessage(
        @Column
        val text: String,
        @Column
        val gameId: String,
        @Column
        val userName: String
) {
    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    var id: String = ""
}
