package be.kdg.boardgame.game._chat.dto

data class MessageDTO(val name: String, val content: String, val gameId: String)
