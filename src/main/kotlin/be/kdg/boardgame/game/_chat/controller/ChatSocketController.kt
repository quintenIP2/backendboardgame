package be.kdg.boardgame.game._chat.controller

import be.kdg.boardgame.authentication.services.UserService
import be.kdg.boardgame.game._chat.dto.MessageDTO
import be.kdg.boardgame.game._chat.service.MessageService
import be.kdg.boardgame.game._game.service.GameService
import be.kdg.boardgame.notifications.service.NotificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller

@Controller
class ChatSocketController {
    @Autowired
    lateinit var template: SimpMessagingTemplate
    @Autowired
    lateinit var messageService: MessageService
    @Autowired
    lateinit var notifService: NotificationService
    @Autowired
    lateinit var userService: UserService
    @Autowired
    lateinit var gameService: GameService

    @MessageMapping("/chat/{gameId}/send")
    @SuppressWarnings("")
    fun onReceivedMesage(message: MessageDTO) {
        val gameId = message.gameId
        println("game id: ${message.gameId}")
        println("message: ${message.content}")
        println("message received from ${message.name} " +
                "in game ${message.gameId} " +
                "with message ${message.content}")

        // send notif about message
        if (message.name != "scrabble") {
            val game = gameService.getGameById(gameId)
            val users = userService.findAllUsersByGame(game).map { it.username }.toMutableList()
            notifService.createNotification(
                    users,
                    gameId,
                    "New Message",
                    "${message.name} has sent a message in ${game.title}",
                    excludeUsers = mutableListOf(message.name))
        }

        // send chatmessage
        messageService.save(message.gameId, message.content, message.name)
        this.template.convertAndSend("/chat/receive/${message.gameId}", message)
    }
}

