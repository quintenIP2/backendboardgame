package be.kdg.boardgame.game._chat.controller

import be.kdg.boardgame.game._chat.dto.MessageDTO
import be.kdg.boardgame.game._chat.service.MessageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class ChatController {
    @Autowired
    lateinit var messageService: MessageService

    /**
     * fetches the history of chatmessages belonging to a certain game
     */
    @GetMapping("/chats/{gameId}/messages")
    fun getAllMessagesForGame(authentication: Authentication, @PathVariable gameId: String
    ): ResponseEntity<List<MessageDTO>> {
        if (authentication.isAuthenticated) {
            val messages = messageService.getChatMessagesByGame(gameId)
                    .map { cm ->
                        MessageDTO(cm.userName, cm.text, cm.gameId)
                    }
            messages.forEach { println(it) }
            return ResponseEntity.ok(messages)
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null)
    }

}
