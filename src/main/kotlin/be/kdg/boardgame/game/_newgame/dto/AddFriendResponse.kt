package be.kdg.boardgame.game._newgame.dto

data class AddFriendResponse (
        val message: String = ""
)
