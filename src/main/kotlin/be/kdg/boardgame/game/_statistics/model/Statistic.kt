package be.kdg.boardgame.game._statistics.model

import be.kdg.boardgame.authentication.models.User
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

/**
 * A [User]'s _statistics for the game.
 *
 * @property player the [User] linked to these _statistics.
 * @property gamesPlayed the amount of games played by the [User].
 * @property gamesWon the amount of games won by the [User].
 * @property avgScore the [User]'s average score, looking at all his games.
 * @property maxScore the [User]'s maximum score throughout all his games.
 * @constructor Creates new _statistics linked to a given [User].
 * @author Nick Van Osta & Quinten Spillemaeckers
 */
@Entity
@Table(name = "tbl_statistic")
data class Statistic(
        @OneToOne
        var player: User,
        var gamesPlayed: Int = 0,
        var gamesWon: Int = 0,
        var avgScore: Double = .0,
        var maxScore: Int = 0
) {
    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    var id: String? = null
}
