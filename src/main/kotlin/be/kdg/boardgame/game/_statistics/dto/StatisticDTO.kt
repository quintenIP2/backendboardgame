package be.kdg.boardgame.game._statistics.dto

import be.kdg.boardgame.authentication.dto.UserDTO

class StatisticDTO(
        val player: UserDTO,
        val gamesPlayed: Int,
        val gamesWon: Int,
        val avgScore: Double,
        val maxScore: Int
)