package be.kdg.boardgame.game._statistics.service

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._statistics.model.Statistic
import org.springframework.stereotype.Service

@Service
interface StatisticService {
    fun getStatistics(user: User): Statistic
    fun updateStatistics(user: User): Statistic
}
