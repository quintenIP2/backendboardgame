package be.kdg.boardgame.game._statistics.controller

import be.kdg.boardgame.authentication.dto.UserDTO
import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.authentication.services.UserService
import be.kdg.boardgame.game._game.model.Score
import be.kdg.boardgame.game._game.service.ScoreService
import be.kdg.boardgame.game._statistics.dto.StatisticDTO
import be.kdg.boardgame.game._statistics.model.Statistic
import be.kdg.boardgame.game._statistics.service.StatisticService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.math.RoundingMode
import java.text.DecimalFormat

@RestController
@RequestMapping("api/")
class StatisticsController {
    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var statisticService: StatisticService

    @GetMapping("statistics")
    fun getStatistics(authentication: Authentication): ResponseEntity<StatisticDTO> {
        if (authentication.isAuthenticated) {
            val user: User = userService.findUserByEmail(authentication.name)
            val updatedStat = statisticService.updateStatistics(user)
            return ResponseEntity.ok(StatisticDTO(
                    UserDTO(user.username, user.email, user.avatar),
                    updatedStat.gamesPlayed,
                    updatedStat.gamesWon,
                    updatedStat.avgScore,
                    updatedStat.maxScore
            ))
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null)
    }
}
