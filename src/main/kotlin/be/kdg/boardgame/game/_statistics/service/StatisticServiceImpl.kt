package be.kdg.boardgame.game._statistics.service

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._game.exceptions.StatisticNotFoundException
import be.kdg.boardgame.game._game.model.Score
import be.kdg.boardgame.game._game.service.ScoreService
import be.kdg.boardgame.game._statistics.model.Statistic
import be.kdg.boardgame.game._statistics.persistence.StatisticRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class StatisticServiceImpl : StatisticService {
    @Autowired
    private lateinit var statisticRepository: StatisticRepository
    @Autowired
    private lateinit var scoreService: ScoreService

    override fun getStatistics(user: User): Statistic {
        return statisticRepository.findByPlayer(user)
                ?: throw StatisticNotFoundException("No statistic found for user '${user.username}'")
    }

    override fun updateStatistics(user: User): Statistic {
        val statistic: Statistic = statisticRepository.findByPlayer(user)
                ?: throw StatisticNotFoundException("No statistic found for user '${user.username}'")
        val scores = scoreService.getScoresByUser(user)

        statistic.gamesPlayed = user.games.size
        statistic.gamesWon = gamesWon(user)
        statistic.avgScore = averageScore(scores)
        statistic.maxScore = maxScore(scores)
        return statisticRepository.save(statistic)
    }

    fun maxScore(scores: MutableList<Score>): Int {
        return scores.map { score -> score.score }.max() ?: 0
    }

    fun averageScore(scores: MutableList<Score>): Double {
        if (scores.size == 0) return .0
        return scores.map { score -> score.score }.average()
    }

    fun gamesWon(user: User): Int {
        return user.games.filter { game -> game.winner == user.username }.size
    }
}
