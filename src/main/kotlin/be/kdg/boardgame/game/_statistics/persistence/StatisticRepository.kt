package be.kdg.boardgame.game._statistics.persistence

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._statistics.model.Statistic
import org.springframework.data.jpa.repository.JpaRepository

interface StatisticRepository: JpaRepository<Statistic, String> {
    fun findByPlayer(user: User): Statistic?
}