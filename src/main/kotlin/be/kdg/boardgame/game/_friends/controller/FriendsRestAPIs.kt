package be.kdg.boardgame.game._friends.controller

import be.kdg.boardgame.authentication.dto.UserDTO
import be.kdg.boardgame.authentication.persistence.UserRepository
import be.kdg.boardgame.game._friends.dto.FriendDto
import be.kdg.boardgame.game._friends.exception.NoFriendsFoundException
import be.kdg.boardgame.game._newgame.dto.AddFriendResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/")
class FriendsRestAPIs(val userRepository: UserRepository) {

    @ExceptionHandler(NoFriendsFoundException::class)
    @GetMapping("friends")
    fun getFriends(auth: Authentication): List<UserDTO>? {
        return userRepository
                .findByEmail(auth.name)
                .orElseThrow { NoFriendsFoundException("This user has no friends added to his friendslist. Sad.") }
                .friends.toList().map { UserDTO(it.username, it.email, it.avatar, it.accountIsActivated) }
    }


    @PostMapping("friends")
    fun addFriend(auth: Authentication, @RequestBody addFriend: FriendDto): ResponseEntity<Any>? {

        if (auth.isAuthenticated) {
            println("friend to add:${addFriend.username}")
            val newFriend = userRepository.findByUsername(addFriend.username).orElseThrow { UsernameNotFoundException("No user found!") }
            val userThatAdds = userRepository.findByEmail(auth.name).orElseThrow { UsernameNotFoundException("No user found!") }

            if (newFriend.email == userThatAdds.email)
                return ResponseEntity(AddFriendResponse("You cannot add yourself as a friend"), HttpStatus.BAD_REQUEST)

            if (userThatAdds.friends.contains(newFriend))
                return ResponseEntity(AddFriendResponse("Already a friend"), HttpStatus.BAD_REQUEST)

            userThatAdds.friends.add(newFriend)
            userRepository.save(userThatAdds)
            return ResponseEntity(AddFriendResponse("${newFriend.username} added by ${userThatAdds.username}!"), HttpStatus.OK)
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Authenticate yourself before requesting this service!")
    }
}
