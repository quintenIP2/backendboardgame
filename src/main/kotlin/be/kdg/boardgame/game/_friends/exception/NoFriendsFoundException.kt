package be.kdg.boardgame.game._friends.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class NoFriendsFoundException(message: String?) : RuntimeException(message)

