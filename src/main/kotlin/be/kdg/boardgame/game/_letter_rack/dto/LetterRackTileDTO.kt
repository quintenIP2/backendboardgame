package be.kdg.boardgame.game._letter_rack.dto

data class LetterRackTileDTO(val id: String? = "", val char: Char = '_')
