package be.kdg.boardgame.game._letter_rack.exceptions

import java.lang.RuntimeException

class LetterNotFoundException(message: String? = "", cause: Throwable? = null)
    : RuntimeException(message, cause)
