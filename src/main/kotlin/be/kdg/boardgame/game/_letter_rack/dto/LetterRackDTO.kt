package be.kdg.boardgame.game._letter_rack.dto

data class LetterRackDTO(
        val player: String,
        val game: String?,
        val tiles: List<LetterRackTileDTO>
)
