package be.kdg.boardgame.game._letter_rack.exceptions

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.util.*

@ControllerAdvice
class LetterRackExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(LetterRackNotFoundException::class)
    fun handleLetterRackNotFoundException(
            ex: LetterRackNotFoundException,
            request: WebRequest
    ): ResponseEntity<LetterRackErrorDetails> {
        val errorDetails = LetterRackErrorDetails(Date(), ex.message ?: "No message", request.getDescription(false))
        return ResponseEntity(errorDetails, HttpStatus.NO_CONTENT)
    }

    @ExceptionHandler(TileNotFoundException::class)
    fun handleTileNotFoundException(
            ex: TileNotFoundException,
            request: WebRequest
    ) : ResponseEntity<TileErrorDetails> {
        val errorDetails = TileErrorDetails(ex.message ?: "No message", request.getDescription(false))
        return ResponseEntity(errorDetails, HttpStatus.NO_CONTENT)
    }
}
