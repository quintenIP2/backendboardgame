package be.kdg.boardgame.game._letter_rack.services

import be.kdg.boardgame.game._letter_rack.exceptions.LetterNotFoundException
import be.kdg.boardgame.game._letter_rack.models.Letter
import be.kdg.boardgame.game._letter_rack.persistence.LetterRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class LetterServiceImpl
    : LetterService {
    @Autowired
    private lateinit var letterRepository: LetterRepository

    override fun getLetterById(id: String): Letter {
        return letterRepository.findById(id)
                .orElseThrow { LetterNotFoundException("No letter with given ID '$id'") }
    }

    override fun getLetterByCharAndLanguage(char: Char, language: String): Letter {
        return letterRepository.findByCharAndLanguage(char, language)
                ?: throw LetterNotFoundException("No letter for given character '$char' and language '$language'")
    }

    override fun getAllLettersByLanguage(language: String): List<Letter> {
        return letterRepository.findAllByLanguage(language)
                .ifEmpty { throw LetterNotFoundException("No letters for given language '$language'") }
    }
}
