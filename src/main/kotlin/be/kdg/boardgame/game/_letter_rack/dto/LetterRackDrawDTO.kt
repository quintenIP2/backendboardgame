package be.kdg.boardgame.game._letter_rack.dto

data class LetterRackDrawDTO(
        val amount: Int
)
