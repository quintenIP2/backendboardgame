package be.kdg.boardgame.game._letter_rack.dto

data class LetterRackUpdateDTO(
        val letters: List<Char>
)
