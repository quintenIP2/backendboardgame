package be.kdg.boardgame.game._letter_rack.exceptions

class TileErrorDetails (
        val message: String,
        val details: String
)
