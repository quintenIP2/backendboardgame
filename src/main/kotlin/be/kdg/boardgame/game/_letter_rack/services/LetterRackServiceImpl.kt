package be.kdg.boardgame.game._letter_rack.services

import be.kdg.boardgame.authentication.services.UserService
import be.kdg.boardgame.game._game.service.GameService
import be.kdg.boardgame.game._letter_rack.exceptions.LetterRackNotFoundException
import be.kdg.boardgame.game._letter_rack.models.LetterRack
import be.kdg.boardgame.game._letter_rack.persistence.LetterRackRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import kotlin.math.min

@Service
class LetterRackServiceImpl : LetterRackService {
    @Autowired
    private lateinit var letterRackRepository: LetterRackRepository
    @Autowired
    private lateinit var gameService: GameService
    @Autowired
    private lateinit var userService: UserService

    private val maxTilesOnLetterRack = 7

    override fun createLetterRackForPlayer(email: String, gameId: String, playerEmail: String): LetterRack {
        val game = gameService.getGameById(gameId)
        val user = userService.findUserByEmail(playerEmail)
        return letterRackRepository.save(
                LetterRack(user, game, gameService.drawTiles(email, gameId, maxTilesOnLetterRack).toMutableList())
        )
    }

    override fun getLetterRackForPlayerInGame(email: String, gameId: String): LetterRack {
        val player = userService.findUserByEmail(email)
        val game = gameService.getGameById(gameId)
        return letterRackRepository.findByGameAndPlayer(game, player)
                ?: throw LetterRackNotFoundException("Letterrack not found for player '$email' in game '$gameId'")
    }

    override fun removeTiles(email: String, gameId: String, letters: Array<Char>): LetterRack {
        val rack = getLetterRackForPlayerInGame(email, gameId)

        letters.forEach { c ->
            rack.tiles.remove(
                    rack.tiles.firstOrNull { t ->
                        t.letter.char == c
                    }
            )
        }
        letterRackRepository.save(rack)

        return rack
    }

    override fun replaceTiles(email: String, gameId: String, letters: Array<Char>): LetterRack {
        var rack = getLetterRackForPlayerInGame(email, gameId)
        val game = gameService.getGameById(gameId)

        for (c in letters) {
            val tile = rack.tiles.firstOrNull { t -> t.letter.char == c } ?: continue

            rack.tiles.remove(tile)
            game.letterBag.offer(tile)
        }

        rack = addRandomTiles(email, gameId, letters.size)

        letterRackRepository.save(rack)

        return rack
    }

    override fun addRandomTiles(email: String, gameId: String, amount: Int): LetterRack {
        val rack = getLetterRackForPlayerInGame(email, gameId)

        rack.tiles.addAll(
                gameService.drawTiles(
                        email, gameId,
                        min(amount, maxTilesOnLetterRack - rack.tiles.size)
                )
        )

        letterRackRepository.save(rack)

        return rack
    }
}
