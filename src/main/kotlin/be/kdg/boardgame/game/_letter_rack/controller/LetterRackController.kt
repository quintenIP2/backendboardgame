package be.kdg.boardgame.game._letter_rack.controller

import be.kdg.boardgame.game._letter_rack.dto.*
import be.kdg.boardgame.game._letter_rack.services.LetterRackService
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/")
class LetterRackController(
        private val letterRackService: LetterRackService
) {
    @GetMapping("letterracks/{gameId}")
    fun getLetterRack(
            authentication: Authentication,
            @PathVariable gameId: String
    ): ResponseEntity<LetterRackDTO> {
        val letterRack = letterRackService.getLetterRackForPlayerInGame(authentication.name, gameId)
        return ResponseEntity.ok(LetterRackDTO(
                letterRack.player.username,
                letterRack.game.title,
                letterRack.tiles.map {
                    LetterRackTileDTO(it.id, it.letter.char)
                }
        ))
    }

    @DeleteMapping("letterracks/{gameId}/tiles")
    fun removeTiles(
            authentication: Authentication,
            @PathVariable gameId: String,
            @RequestBody updateDTO: LetterRackUpdateDTO
    ): ResponseEntity<LetterRackDTO> {
        val result = letterRackService.removeTiles(authentication.name, gameId, updateDTO.letters.toTypedArray())
        return ResponseEntity.ok(LetterRackDTO(
                result.player.username,
                result.game.title,
                result.tiles.map {
                    LetterRackTileDTO(it.id, it.letter.char)
                }
        ))
    }

    @PutMapping("letterracks/{gameId}/tiles/replace")
    fun replaceTiles(
            authentication: Authentication,
            @PathVariable gameId: String,
            @RequestBody updateDTO: LetterRackUpdateDTO
    ): ResponseEntity<LetterRackDTO> {
        val result = letterRackService.replaceTiles(authentication.name, gameId, updateDTO.letters.toTypedArray())
        return ResponseEntity.ok(LetterRackDTO(
                result.player.username,
                result.game.title,
                result.tiles.map {
                    LetterRackTileDTO(it.id, it.letter.char)
                }
        ))
    }

    @PostMapping("letterracks/{gameId}/tiles/random")
    fun addRandomTiles(
            authentication: Authentication,
            @PathVariable gameId: String,
            @RequestBody updateDTO: LetterRackDrawDTO
    ): ResponseEntity<LetterRackDTO> {
        val result = letterRackService.addRandomTiles(authentication.name, gameId, updateDTO.amount)
        return ResponseEntity.ok(LetterRackDTO(
                result.player.username,
                result.game.title,
                result.tiles.map {
                    LetterRackTileDTO(it.id, it.letter.char)
                }
        ))
    }
}
