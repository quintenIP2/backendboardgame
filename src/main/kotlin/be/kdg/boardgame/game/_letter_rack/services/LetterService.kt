package be.kdg.boardgame.game._letter_rack.services

import be.kdg.boardgame.game._letter_rack.models.Letter

interface LetterService {
    /**
     * Get [Letter] by its id.
     * @param id [Letter]'s id.
     * @return The requested [Letter].
     * @author Nick Van Osta
     */
    fun getLetterById(id: String): Letter

    /**
     * Get [Letter] by its character and language.
     * @param char [Letter]'s character.
     * @param language [Letter]'s language.
     * @return The requested [Letter].
     * @author Nick Van Osta
     */
    fun getLetterByCharAndLanguage(char: Char, language: String): Letter

    /**
     * Get all [Letter]s for a language.
     * @param language [Letter]'s language.
     * @return List of [Letter]s for the language.
     * @author Nick Van Osta
     */
    fun getAllLettersByLanguage(language: String): List<Letter>
}
