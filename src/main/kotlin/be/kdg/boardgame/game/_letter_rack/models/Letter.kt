package be.kdg.boardgame.game._letter_rack.models

import be.kdg.boardgame.game._board.models.Tile
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

/**
 * A Scrabble letter with the associated score depending on the set language.
 *
 * @property char a Char representing the letter.
 * @property language the language defining the [score]
 * @property score the amount of points this letter is worth.
 * @constructor Creates a letter with a given score for a given language.
 * @author Nick Van Osta & Quinten Spillemaeckers
 */
@Entity
@Table(name = "tbl_letter")
data class Letter(
        @Column
        val char: Char,
        @Column
        val language: String,
        @Column
        val score: Int,
        @OneToMany(cascade = [CascadeType.ALL])
        val tiles: MutableList<Tile> = mutableListOf()
) {
    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    var id: String? = null
}
