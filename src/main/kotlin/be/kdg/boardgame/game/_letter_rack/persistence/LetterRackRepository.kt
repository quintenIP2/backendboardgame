package be.kdg.boardgame.game._letter_rack.persistence

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._letter_rack.models.LetterRack
import be.kdg.boardgame.game._game.model.Game
import org.springframework.data.jpa.repository.JpaRepository

interface LetterRackRepository : JpaRepository<LetterRack, String?> {
    fun findByGameAndPlayer(game: Game, player: User): LetterRack?
}
