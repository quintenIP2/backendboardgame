package be.kdg.boardgame.game._letter_rack.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NO_CONTENT, code = HttpStatus.NO_CONTENT, reason = "")
class TileNotFoundException(
        message: String? = "",
        cause: Throwable? = null
) : RuntimeException(message, cause)
