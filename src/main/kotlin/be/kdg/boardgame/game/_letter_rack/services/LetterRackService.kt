package be.kdg.boardgame.game._letter_rack.services

import be.kdg.boardgame.game._letter_rack.models.LetterRack

interface LetterRackService {
    /**
     * Create the [LetterRack] for a [User] in a [Game].
     * @param email [Game] creator's email address.
     * @param gameId [Game]'s id.
     * @param playerEmail: [User] who needs their [LetterRack] created.
     * @return The newly created [LetterRack].
     * @author Nick Van Osta
     */
    fun createLetterRackForPlayer(email: String, gameId: String, playerEmail: String): LetterRack

    /**
     * Get [LetterRack] of [User] in [Game].
     * @param email [User]'s email address.
     * @param gameId [Game]'s id.
     * @return The requested [LetterRack].
     * @author Nick Van Osta
     */
    fun getLetterRackForPlayerInGame(email: String, gameId: String): LetterRack

    /**
     * Remove [Tile]s from [User]'s [LetterRack].
     * @param email [User]'s email address.
     * @param gameId [Game]'s id.
     * @param letters The list of [Letter]s that need to be removed.
     * @return The updated [LetterRack].
     * @author Nick Van Osta
     */
    fun removeTiles(email: String, gameId: String, letters: Array<Char>): LetterRack

    /**
     * Replace [Tile]s from [User]'s [LetterRack].
     * @param email [User]'s email address.
     * @param gameId [Game]'s id.
     * @param letters The list of [Letter]s that need to be replaced.
     * @return The updated [LetterRack].
     * @author Nick Van Osta
     */
    fun replaceTiles(email: String, gameId: String, letters: Array<Char>): LetterRack

    /**
     * Draw [Tile]s from the [Game]'s letter bag and insert them into the [User]'s [LetterRack].
     * @param email [User]'s email address.
     * @param gameId [Game]'s id.
     * @param amount Amount of [Tile]s to draw from the letter bag.
     * @return The updated [LetterRack].
     * @author Nick Van Osta
     */
    fun addRandomTiles(email: String, gameId: String, amount: Int): LetterRack
}
