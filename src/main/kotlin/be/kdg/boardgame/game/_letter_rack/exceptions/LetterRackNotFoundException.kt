package be.kdg.boardgame.game._letter_rack.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.RuntimeException

@ResponseStatus(value = HttpStatus.NO_CONTENT, code = HttpStatus.NO_CONTENT, reason = "")
class LetterRackNotFoundException(
        override val message: String? = null,
        override val cause: Throwable? = null
) : RuntimeException(message, cause)
