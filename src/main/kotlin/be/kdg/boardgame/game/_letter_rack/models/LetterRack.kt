package be.kdg.boardgame.game._letter_rack.models

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._game.model.Game
import javax.persistence.*

/**
 * @property player the [User] to which this rack belongs.
 * @property tiles the [Tile]s on this rack.
 * @constructor Creates an empty rack for a given [User].
 * @author Nick Van Osta & Quinten Spillemaeckers
 */

@Entity
@Table(name = "tbl_letterrack")
data class LetterRack(
        @OneToOne
        var player: User,

        @ManyToOne
        var game: Game,

        @OneToMany(cascade = [CascadeType.ALL])
        var tiles: MutableList<Tile> = mutableListOf()
){
    @Id
    @GeneratedValue(generator = "uuid2")
    var id: String? = null
}
