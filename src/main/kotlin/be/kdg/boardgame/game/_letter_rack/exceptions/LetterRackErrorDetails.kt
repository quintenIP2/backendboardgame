package be.kdg.boardgame.game._letter_rack.exceptions

import java.util.*

data class LetterRackErrorDetails(
        val timestamp: Date,
        val message: String,
        val details: String
)
