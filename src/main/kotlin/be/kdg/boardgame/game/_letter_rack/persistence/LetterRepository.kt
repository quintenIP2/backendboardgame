package be.kdg.boardgame.game._letter_rack.persistence

import be.kdg.boardgame.game._letter_rack.models.Letter
import org.springframework.data.jpa.repository.JpaRepository

interface LetterRepository
    : JpaRepository<Letter, String> {
    fun findByCharAndLanguage(char: Char, language: String): Letter?
    fun findAllByLanguage(language: String): List<Letter>
}
