package be.kdg.boardgame.game._game.service

import be.kdg.boardgame.authentication.dto.UserDTO
import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.authentication.persistence.UserRepository
import be.kdg.boardgame.authentication.services.UserService
import be.kdg.boardgame.dbseed.letters.frequencies.LetterDistributionInitializer
import be.kdg.boardgame.game._board.exceptions.IllegalMoveException
import be.kdg.boardgame.game._board.models.Board
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._board.service.BoardService
import be.kdg.boardgame.game._game.dto.LaidDownTileDTO
import be.kdg.boardgame.game._game.dto.PlayedTurnDTO
import be.kdg.boardgame.game._game.dto.WordScoreDTO
import be.kdg.boardgame.game._game.exceptions.GameNotFoundException
import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._game.model.Invitation
import be.kdg.boardgame.game._game.model.Turn
import be.kdg.boardgame.game._game.persistence.GameRepository
import be.kdg.boardgame.game._game.persistence.InvitationRepository
import be.kdg.boardgame.game._letter_rack.models.LetterRack
import be.kdg.boardgame.game._letter_rack.services.LetterRackService
import be.kdg.boardgame.game._overview.dto.GameOverviewDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import java.util.*


@Service
class GameServiceImpl : GameService {

    @Autowired
    lateinit var template: SimpMessagingTemplate
    @Autowired
    private lateinit var gameRepository: GameRepository
    @Autowired
    private lateinit var userRepository: UserRepository
    @Autowired
    private lateinit var invitationRepo: InvitationRepository
    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var boardService: BoardService
    @Autowired
    private lateinit var turnService: TurnService
    @Autowired
    private lateinit var letterRackService: LetterRackService
    @Autowired
    private lateinit var letterDistributionInitializer: LetterDistributionInitializer
    @Autowired
    private lateinit var validationService: ValidationService
    @Autowired
    private lateinit var scoreService: ScoreService

    @Throws(Exception::class)
    override fun playTiles(email: String, gameId: String, cellsToUpdate: List<LaidDownTileDTO>): PlayedTurnDTO {
        val game = getGameById(gameId)
        val user = userService.findUserByEmail(email)
        val rack = letterRackService.getLetterRackForPlayerInGame(email, gameId)
        val board = game.board ?: throw IllegalMoveException("The game '${game.id}' has no board to play on yet")
        val usernames = userService.findAllUsersByGame(game).map { it.username }.toMutableList()

        // check if skipped turn
        if (checkIfSkip(game, cellsToUpdate) > 0) {
            // check for game over
            val gameOver = checkIfGameOver(game, rack)
            saveTurn(game, user, 0)
            game.currentlyPlaying = findNextPlayer(game.currentlyPlaying, usernames)
            gameRepository.save(game)
            return PlayedTurnDTO(listOf(), "", game.board.toString(), gameOver)
        }

        // lay tiles on board
        val tiles = layTilesOnBoard(board, rack, cellsToUpdate)

        // validate
        val turnDTO = validate(board, tiles)

        // remove tiles from letter rack
        removeFromLetterRack(email, game, rack, cellsToUpdate)

        // check if game over
        turnDTO.over = checkIfGameOver(game, rack)

        // find next player
        game.currentlyPlaying = findNextPlayer(game.currentlyPlaying, usernames)

        // update scores
        val score = updateScores(game, user, turnDTO.words)

        gameRepository.save(game)

        // save turn
        saveTurn(game, user, score)

        return turnDTO
    }

    override fun deleteGame(id: String, creatorEmail: String): Boolean {
        val game = gameRepository.findById(id)
        val user = userRepository.findByEmail(creatorEmail)
        if (!game.isPresent) throw GameNotFoundException("Tried to look up game: $id, but it was not found")
        if (!user.isPresent) throw UsernameNotFoundException("Tried to look for username: $creatorEmail, but it was not found")

        // if not present already deleted, just return true
        val deleted = !gameRepository.findById(id).isPresent
        if (deleted) return true

        // delete cascading relations that dont need to be removed from game first
        println("deleting game $id")

        //delete FK relations
        game.get().board?.cells = mutableListOf()
        game.get().board = null
        game.get().invitations = mutableListOf()
        game.get().racks = mutableListOf()
        game.get().letterBag = ArrayDeque()
        game.get().scores = mutableListOf()
        game.get().turns = mutableListOf()
        gameRepository.save(game.get())

        //update username
        user.get().games.remove(game.get())
        userRepository.save(user.get())

        //then delete game
        gameRepository.delete(game.get())

        return !gameRepository.findById(id).isPresent
    }

    override fun getGameByName(email: String, title: String): Game {
        val user = userService.findUserByEmail(email)
        val game = gameRepository.findByCreatorAndTitle(user, title)
        return game ?: throw GameNotFoundException("Game not found!")
    }

    override fun getGameById(id: String): Game {
        val optionalGame = gameRepository.findById(id)
        if (optionalGame.isPresent) {
            return optionalGame.get()
        }
        throw GameNotFoundException("Game not found!")
    }

    /**
     * [createGame] creates a new game with a generated default board and with players that are invited
     * invitations are declined by default. They'll be set to true when the player accepts his invitation.
     */
    override fun createGame(creator: String, title: String, language: String, players: MutableList<String>): Game {
        val creatorUser = userService.findUserByEmail(creator)
        val otherPlayers = getRequestedPlayers(creatorUser, players)

        // assemble new game
        val newGame = Game(
                creatorUser,
                title,
                language,
                mutableListOf(),
                currentlyPlaying = (0 until otherPlayers.size).random(),
                winner = "")

        // board
        newGame.board = boardService.generateBoard(newGame)

        // add games to players
        otherPlayers.forEach { p -> p.games.add(newGame); userService.save(p) }

        // add all players to game
        newGame.users.addAll(otherPlayers)

        // create invitations for each player on false except for the creator who immediately accepts his created game
        val invs = newGame.users.map {
            if (it.username != newGame.creator.username)
                invitationRepo.save(Invitation(it, newGame, false))
            else
                invitationRepo.save(Invitation(it, newGame, true))
        }
        newGame.invitations.addAll(invs)

        return gameRepository.save(newGame)
    }

    override fun drawTiles(email: String, gameId: String, amount: Int): List<Tile> {
        val game = getGameById(gameId)

        if (game.letterBag.isEmpty()) initLetterBag(game)

        val tiles = mutableListOf<Tile>()

        for (i in 0 until amount) {
            val tile = game.letterBag.poll() ?: return tiles
            tiles.add(tile)
        }

        return tiles
    }

    override fun addPlayer(creator: String, gameId: String, email: String): Game {
        val game = getGameById(gameId)
        game.users.add(userService.findUserByEmail(email))
        return gameRepository.save(game)
    }

    override fun handleInvitationResponses(email: String, gameId: String, hasAccepted: Boolean): Boolean {
        // save
        val user = userService.findUserByEmail(email)
        val game = gameRepository.findById(gameId).orElseThrow { GameNotFoundException("Game Not Found with id : $gameId") }
        val inv = game.invitations.first { it.user.email == email }
        inv.hasAccepted = true
        invitationRepo.save(inv)
        gameRepository.save(game)

        // send newGame to overview of player that has accepted
        val newGameDTO = GameOverviewDTO(
                game.id,
                game.title,
                game.currentlyPlaying,
                game.users.map { player ->
                    UserDTO(player.username, player.email, player.avatar)
                },
                UserDTO(game.creator.username, game.creator.email, game.creator.avatar),
                game.isOver,
                game.hasStarted,
                game.winner,
                turnService.getAllTurnsByGameId(game.id).size
        )

        this.template.convertAndSend("/games/new/receive/${user.username}", newGameDTO)

        // check if game can be started and send overviewDTO
        val allInvitationsAccepted = game.invitations.map { it.hasAccepted }.reduce { acc, b -> acc && b }
        if (allInvitationsAccepted) {
            // start game
            val startedGame = startGame(email, game.id) // initialize game

            // send overviewGame to frontend trough websocket to update overview
            val startedGameDTO = GameOverviewDTO(
                    startedGame.id,
                    startedGame.title,
                    startedGame.currentlyPlaying,
                    startedGame.users.map { player ->
                        UserDTO(player.username, player.email, player.avatar)
                    },
                    UserDTO(startedGame.creator.username, startedGame.creator.email, startedGame.creator.avatar),
                    startedGame.isOver,
                    startedGame.hasStarted,
                    startedGame.winner,
                    turnService.getAllTurnsByGameId(game.id).size
            )

            startedGame.users.forEach {
                this.template.convertAndSend("/games/start/receive/${it.username}", startedGameDTO)
            }
            return true
        }
        return false
    }

    override fun startGame(email: String, gameId: String): Game {
        var game = getGameById(gameId)

        if (!game.hasStarted) {
            game.users.forEach {
                game.racks.add(letterRackService.createLetterRackForPlayer(email, gameId, it.email))
            }
            game = scoreService.initializeScores(game)
        }

        game.hasStarted = true
        return gameRepository.save(game)
    }

    override fun endGame(gameId: String): Game {
        val game = getGameById(gameId)
        game.isOver = true

        calcFinalScores(game)

        // determine winner
        val scores = scoreService.getScoresByGame(game.id)
        val winner = scores.sortedBy { it.score }.toMutableList().last().user.username
        game.winner = winner

        return gameRepository.save(game)
    }

    private fun calcFinalScores(game: Game) {
        var sumScores = 0
        val zipped = game.scores.zip(game.racks)

        zipped.forEach {
            // get sum of scores of tiles in letterrack and subtract from player score
            val score = it.second.tiles.map { t -> t.letter.score }.fold(0) { a, b -> a + b }
            it.first.score = scoreService.incrementScore(game, it.second.player, -score).score

            // add score to sum
            sumScores += score
        }

        // find player with empty letterrack and add sum of scores to their score
        val emptyRack = zipped.find { it.second.tiles.size <= 0 } ?: return
        emptyRack.first.score = scoreService.incrementScore(game, emptyRack.second.player, sumScores).score
    }

    private fun checkIfSkip(game: Game, tiles: List<LaidDownTileDTO>): Int {
        if (tiles.isEmpty())
            game.skippedTurns++
        else
            game.skippedTurns = 0

        return game.skippedTurns
    }

    private fun checkIfGameOver(game: Game, rack: LetterRack): Boolean {
        val gameOver = game.skippedTurns >= game.users.size * 2 || (game.letterBag.size <= 0 && rack.tiles.size <= 0)

        if (gameOver) {
            val endedGame = endGame(game.id)
            println("WINNER OF GAME ${endedGame.title} = ${endedGame.winner} ")
            game.skippedTurns = 0
        }

        return gameOver
    }

    private fun layTilesOnBoard(board: Board, rack: LetterRack, tiles: List<LaidDownTileDTO>): List<Tile> {
        val tempRackTiles = rack.tiles.toMutableList()
        return tiles.map {
            val tile = tempRackTiles.firstOrNull { t ->
                t.letter.char == it.char
            } ?: throw IllegalMoveException("The player has no tile with letter '${it.char}' in their letterrack")
            tempRackTiles.remove(tile)
            tile.cell = board.cells[it.y * board.width + it.x]
            return@map tile
        }
    }

    private fun removeFromLetterRack(email: String, game: Game, rack: LetterRack, tiles: List<LaidDownTileDTO>) {
        val tempRackTiles = rack.tiles.toMutableList()
        tiles.forEach {
            val tile = tempRackTiles.firstOrNull { t ->
                t.letter.char == it.char
            } ?: throw IllegalMoveException("The player has no tile with letter '${it.char}' in their letterrack")

            letterRackService.removeTiles(email, game.id, arrayOf(tile.letter.char))
            tempRackTiles.remove(tile)
            boardService.layTile(game.board?.id ?: "", tile, it.x, it.y)
        }
    }

    private fun validate(board: Board, tiles: List<Tile>): PlayedTurnDTO {
        validationService.validateLaidDownTiles(board, tiles)
        return validationService.scoreLaidDownTiles(board, tiles)
    }

    private fun updateScores(game: Game, user: User, words: List<WordScoreDTO>): Int {
        var score = 0
        words.forEach {
            score = scoreService.incrementScore(game, user, it.score).score
        }
        return score
    }

    private fun saveTurn(game: Game, user: User, score: Int) {
        val racksString = turnService.stringifyPlayerRacks(game.racks)
        val turn = Turn(game, turnService.getAllTurnsByGameId(game.id).size + 1, score, game.board.toString(), user, racksString)
        turnService.save(turn)
    }

    private fun initLetterBag(game: Game) {
        val letters = letterDistributionInitializer.initLetterFrequencies().toMutableList()

        // remove letters that are on the board
        game.board?.cells?.forEach {
            letters.remove(it.tile?.letter)
        }

        // remove letters in the letterracks
        game.racks.forEach { rack ->
            rack.tiles.forEach {
                letters.remove(it.letter)
            }
        }

        letters.shuffle()
        letters.forEach {
            game.letterBag.offer(Tile(it))
        }
    }

    /**
     * possible values from frontend
     * Random: A random player from the community
     * None: An empty player slot to be filled by someone else (...somewhere in the future)
     * Bot: A smart artificial player, is saved in db like a normal username
     * Friend, Previous: A friend or someone you've played against in username form
     *
     * The playertypes get sorted by their type
     */
    fun getRequestedPlayers(creatorUser: User, players: MutableList<String>): MutableList<User> {
        val addedPlayers = mutableListOf(creatorUser)
        println("sorted players ${players.sortedWith(compareBy { it.toLowerCase() }).map { it.toLowerCase() }}")
        players.sortedWith(compareBy { it.toLowerCase() })
                .forEach { u ->
                    when (u.toLowerCase()) {
                        "random" -> {
                            var isUserNotFound = true
                            var randomUser: User
                            while (isUserNotFound) {
                                randomUser = userService.findRandomRow()
                                if (!addedPlayers.contains(randomUser)) {
                                    addedPlayers.add(randomUser)
                                    isUserNotFound = false
                                }
                            }
                        }
                        "none" -> Unit
                        else -> addedPlayers.add(userService.findByUsername(u))
                    }
                }

        println(addedPlayers)
        return addedPlayers
    }


    /** who's next? */
    override fun findNextPlayer(game: Game): Int {
        return (game.currentlyPlaying + 1) % game.users.size
    }

    override fun findNextPlayer(currentlyPlaying: Int, users: MutableList<String>): Int {
        return (currentlyPlaying + 1) % users.size
    }
}
