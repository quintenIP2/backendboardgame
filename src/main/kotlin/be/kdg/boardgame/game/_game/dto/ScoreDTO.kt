package be.kdg.boardgame.game._game.dto


data class ScoreDTO(val gameId: String, val username: String, var score: Int = 0)
