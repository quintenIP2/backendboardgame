package be.kdg.boardgame.game._game.dto

data class NewGameRequestDTO(
        val title: String,
        val language: String,
        val players: MutableList<String>
)
