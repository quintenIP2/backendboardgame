package be.kdg.boardgame.game._game.dto

data class AcceptInvitationDTO(val hasAccepted: Boolean)
