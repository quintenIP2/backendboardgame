package be.kdg.boardgame.game._game.persistence

import be.kdg.boardgame.game._game.model.Word
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface WordRepository
    : JpaRepository<Word, String> {
    @Query("SELECT * FROM tbl_word WHERE word REGEXP ?1 AND language = ?2", nativeQuery = true)
    fun findByWordRegexAndLanguage(word: String, language: String): Collection<Word>
    fun findByWordAndLanguage(word: String, language: String): Word?
    fun findAllByLanguage(language: String): Collection<Word>
}
