package be.kdg.boardgame.game._game.service

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._game.model.Score

interface ScoreService {
    /**
     * Initialize the [Score]s for a given [Game].
     * @param game [Game] whose scores need to be initialized.
     * @return The updated [Game].
     * @author Nick Van Osta
     */
    fun initializeScores(game: Game): Game

    /**
     * Update the [Score] in a certain [Game] for a certain [User].
     * @param game [Game] to be updated.
     * @param user [User] whose [Score] needs to be updated.
     * @param newScore: The new score.
     * @author Nick Van Osta
     */
    fun updateScore(game: Game, user: User, newScore: Int): Score

    /**
     * Increment / decrement the [Score] in a certain [Game] for a certain [User].
     * @param game [Game] to be updated.
     * @param user [User] whose [Score] needs to be updated.
     * @param scoreToAdd: The score to be added to the [User]'s score. You can give a negative number for point deduction.
     * @author Nick Van Osta
     */
    fun incrementScore(game: Game, user: User, scoreToAdd: Int): Score

    /**
     * Get all [Score]s for a [Game].
     * @param gameId [Game]'s id.
     * @return List of [Score]s of the [Game]'s players.
     * @author Nick Van Osta
     */
    fun getScoresByGame(gameId: String): MutableList<Score>

    /**
     * Get the [Score] for a certain [User] in [Game].
     * @param game [Game] to be fetched.
     * @param user [User] whose [Score] to be fetched.
     * @return The [User]'s [Score] in the given [Game].
     * @author Nick Van Osta
     */
    fun getScoreByGameAndPlayer(game: Game, user: User): Score

    /**
     * Get all [Score]s for a [User] across all [Game]s.
     * @param user [User] whose [Score]s to be fetched.
     * @return List of [Score]s of the given [User].
     * @author Nick Van Osta
     */
    fun getScoresByUser(user: User): MutableList<Score>
}
