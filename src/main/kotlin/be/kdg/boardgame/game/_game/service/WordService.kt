package be.kdg.boardgame.game._game.service

import be.kdg.boardgame.game._game.model.Word

interface WordService {
    /**
     * Searches for a given [Word] in the language's dictionary.
     * @param word [Word] to be searched for.
     * @param language The language the search through.
     * @return The found [Word].
     * @author Nick Van Osta
     */
    fun findWordByLanguage(word: String, language: String): Word
}
