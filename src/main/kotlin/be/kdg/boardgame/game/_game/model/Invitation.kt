package be.kdg.boardgame.game._game.model

import be.kdg.boardgame.authentication.models.User
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

/**
 * A Scrabble invitation for a certain game
 *
 * @property user the [User] who this invitation belongs to
 * @property hasAccepted if all invitations are accepted represented by this [Boolean] we can start the game
 * @author Jonathan Peers
 */

@Entity
@Table(name = "tbl_invitation")
data class Invitation(
        @ManyToOne
        var user: User,
        @ManyToOne
        var game: Game,
        @Column
        var hasAccepted: Boolean = false
){
        @Id
        @Column
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "uuid2")
        var id: String = ""
}
