package be.kdg.boardgame.game._game.persistence

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._game.model.Game
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

// get only games that have been accepted
@Repository
interface GameRepository : JpaRepository<Game, String> {
    fun findAllByUsersContains(user: User): List<Game>
    fun findByCreatorAndTitle(user: User, title: String): Game?

}
