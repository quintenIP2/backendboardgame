package be.kdg.boardgame.game._game.service

import be.kdg.boardgame.game._game.model.Turn
import be.kdg.boardgame.game._letter_rack.models.LetterRack

interface TurnService {
    fun getAllTurnsByGameId(id: String): MutableList<Turn>
    fun save(turn: Turn): Turn
    fun stringifyPlayerRacks(letterRacks: MutableList<LetterRack>): String
}
