package be.kdg.boardgame.game._game.exceptions

import be.kdg.boardgame.game._board.exceptions.IllegalMoveException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.util.*

@ControllerAdvice
class GameExceptionHandler :
        ResponseEntityExceptionHandler() {
    @ExceptionHandler(GameNotFoundException::class)
    fun handleLetterRackNotFoundException(
            ex: GameNotFoundException,
            request: WebRequest
    ): ResponseEntity<GameErrorDetails> {
        val errorDetails = GameErrorDetails(Date(), ex.message ?: "No message", request.getDescription(false))
        return ResponseEntity(errorDetails, HttpStatus.NO_CONTENT)
    }

    @ExceptionHandler(IllegalMoveException::class)
    fun handleIllegalMoveException(
            ex: IllegalMoveException,
            request: WebRequest
    ): ResponseEntity<GameErrorDetails> {
        val errorDetails = GameErrorDetails(Date(), ex.message ?: "No message", request.getDescription(false))
        return ResponseEntity(errorDetails, HttpStatus.FORBIDDEN)
    }

    @ExceptionHandler(ScoreNotFoundException::class)
    fun handleScoreNotFoundException(
            ex: ScoreNotFoundException,
            request: WebRequest
    ): ResponseEntity<GameErrorDetails> {
        val errorDetails = GameErrorDetails(Date(), ex.message ?: "No message", request.getDescription(false))
        return ResponseEntity(errorDetails, HttpStatus.NO_CONTENT)
    }
}
