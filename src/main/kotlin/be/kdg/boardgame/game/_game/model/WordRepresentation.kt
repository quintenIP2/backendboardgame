package be.kdg.boardgame.game._game.model

data class WordRepresentation(
        val word: String,
        val score: Int,
        val row: Int,
        val col: Char,
        val vertical: Boolean
) {
    override fun toString(): String {
        val loc: String = if (vertical) "$col$row" else "$row$col"
        return "$word $loc +$score"
    }
}
