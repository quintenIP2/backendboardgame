package be.kdg.boardgame.game._game.dto

data class WordScoreDTO (
        val word: String,
        val score: Int
)
