package be.kdg.boardgame.game._game.exceptions

class ScoreNotFoundException(
        message: String? = "no message",
        cause: Throwable? = null
) : RuntimeException(message, cause)
