package be.kdg.boardgame.game._game.persistence

import be.kdg.boardgame.game._game.model.Turn
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TurnRepository : JpaRepository<Turn, String> {
    fun findAllByGameId(id: String): MutableList<Turn>
}
