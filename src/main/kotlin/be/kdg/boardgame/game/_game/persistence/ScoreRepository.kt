package be.kdg.boardgame.game._game.persistence

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._game.model.Score
import org.springframework.data.jpa.repository.JpaRepository

interface ScoreRepository
    : JpaRepository<Score, String> {
    fun findByGameAndUser(game: Game, user: User): Score?
    fun findByUser(user: User): MutableList<Score>?
    fun findAllByGameId(gameId: String): MutableList<Score>
}
