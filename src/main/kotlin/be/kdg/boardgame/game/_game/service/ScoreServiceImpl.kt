package be.kdg.boardgame.game._game.service

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._game.exceptions.ScoreNotFoundException
import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._game.model.Score
import be.kdg.boardgame.game._game.persistence.ScoreRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ScoreServiceImpl
    : ScoreService {


    @Autowired
    private lateinit var scoreRepository: ScoreRepository

    override fun initializeScores(game: Game): Game {
        game.scores = game.users.map { scoreRepository.save(Score(game, it)) }
        return game
    }

    override fun updateScore(game: Game, user: User, newScore: Int): Score {
        val score = scoreRepository.findByGameAndUser(game, user) ?: throw ScoreNotFoundException("No score found in game '${game.id}' for user '${user.email}'")
        score.score = newScore
        return scoreRepository.save(score)
    }

    override fun incrementScore(game: Game, user: User, scoreToAdd: Int): Score {
        val score = scoreRepository.findByGameAndUser(game, user) ?: throw ScoreNotFoundException("No score found in game '${game.id}' for user '${user.email}'")
        score.score += scoreToAdd
        return scoreRepository.save(score)
    }

    override fun getScoresByGame(gameId: String): MutableList<Score> {
        return scoreRepository.findAllByGameId(gameId)
    }

    override fun getScoreByGameAndPlayer(game: Game, user: User): Score {
        return scoreRepository.findByGameAndUser(game, user) ?: throw ScoreNotFoundException("No score found in game '${game.id}' for user '${user.email}'")
    }

    override fun getScoresByUser(user: User): MutableList<Score> {
        return scoreRepository.findByUser(user) ?: throw ScoreNotFoundException("No score found for user '${user.email}'")
    }
}
