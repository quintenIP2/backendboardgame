package be.kdg.boardgame.game._game.dto

data class LaidDownTileDTO (
        val char: Char,
        val x: Int,
        val y: Int
)
