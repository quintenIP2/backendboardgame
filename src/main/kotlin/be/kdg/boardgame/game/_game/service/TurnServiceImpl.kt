package be.kdg.boardgame.game._game.service

import be.kdg.boardgame.game._game.model.Turn
import be.kdg.boardgame.game._game.persistence.TurnRepository
import be.kdg.boardgame.game._letter_rack.models.LetterRack
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TurnServiceImpl : TurnService {
    @Autowired
    lateinit var turnRepository: TurnRepository

    override fun getAllTurnsByGameId(id: String): MutableList<Turn> {
        return turnRepository.findAllByGameId(id)
    }

    override fun save(turn: Turn): Turn {
        return turnRepository.save(turn)
    }

    override fun stringifyPlayerRacks(letterRacks: MutableList<LetterRack>): String {
        return letterRacks.map {
            it.tiles.map { t -> t.letter.char }.plus('#').joinToString(separator = "") { t -> t.toString() }
        }.joinToString(separator = "") { it }
    }
}
