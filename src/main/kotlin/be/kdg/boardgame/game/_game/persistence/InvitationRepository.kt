package be.kdg.boardgame.game._game.persistence

import be.kdg.boardgame.game._game.model.Invitation
import org.springframework.data.jpa.repository.JpaRepository

interface InvitationRepository : JpaRepository<Invitation, String>
