package be.kdg.boardgame.game._game.exceptions

import java.lang.RuntimeException

class GameNotFoundException(
        message: String? = "",
        cause: Throwable? = null
)
    : RuntimeException(message, cause)
