package be.kdg.boardgame.game._game.dto

import be.kdg.boardgame.authentication.dto.UserDTO

data class ReplayTurnDTO(val gameId: String,
                         val turnCounter: Int,
                         var board: String,
                         val player: String,
                         val score: Int,
                         val racks: String,
                         val users: MutableList<UserDTO>)
