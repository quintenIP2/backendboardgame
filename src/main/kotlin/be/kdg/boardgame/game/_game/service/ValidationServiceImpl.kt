package be.kdg.boardgame.game._game.service

import be.kdg.boardgame.game._board.exceptions.IllegalMoveException
import be.kdg.boardgame.game._board.models.Board
import be.kdg.boardgame.game._board.models.Cell
import be.kdg.boardgame.game._board.models.Multiplier
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._board.service.BoardService
import be.kdg.boardgame.game._game.dto.PlayedTurnDTO
import be.kdg.boardgame.game._game.dto.WordScoreDTO
import be.kdg.boardgame.game._game.model.WordRepresentation
import kotlinx.coroutines.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ValidationServiceImpl
    : ValidationService {

    @Autowired
    private lateinit var boardService: BoardService
    @Autowired
    private lateinit var wordService: WordService

    private val maxTilesInLetterRack = 7

    override fun validateLaidDownTiles(board: Board, tiles: List<Tile>): Boolean {
        runBlocking {
            launch { checkCenterTile(board, tiles) }
            launch { checkLettersInLine(tiles) }
            launch { checkIfConnected(board, tiles) }
        }
        return true
    }

    override fun scoreLaidDownTiles(board: Board, tiles: List<Tile>): PlayedTurnDTO {
        var playedTurn = PlayedTurnDTO(emptyList(), "", "")
        runBlocking {
            val words = findWords(board, tiles)
            var repr = ""

            words.forEach {
                repr += "$it\n"
            }

            clearTilesFromBoard(board, tiles)
            playedTurn = PlayedTurnDTO(words.map { WordScoreDTO(it.word, it.score) }, repr.trim(), board.toString())
        }

        return playedTurn
    }

    /**
     * Remove the given [Tile]s from the [Board].
     * @param board [Board] to be cleared.
     * @param tiles [Tile]s to be removed.
     * @author Nick Van Osta
     */
    private fun clearTilesFromBoard(board: Board, tiles: List<Tile>) {
        tiles.forEach {
            val cell = it.cell ?: throw IllegalMoveException("The tile is not laid on the board")
            boardService.removeTile(board.id ?: "", cell.x, cell.y)
        }
    }

    /**
     * Where the magic happens.
     * Check for formed [Word]s on the given [Board] with the laid down [Tile]s.
     * @param board [Board] to check on.
     * @param tiles [Tile]s to check.
     * @return List of [WordRepresentation]s, containing the formed [Word] and its [Score].
     * @author Nick Van Osta
     */
    private suspend fun findWords(board: Board, tiles: List<Tile>): List<WordRepresentation> {
        val words = mutableListOf<WordRepresentation>()
        val isSingle = tiles.size == 1
        val isHorizontal = isSingle || tiles.all { it.cell?.y == tiles.first().cell?.y }
        val isVertical = isSingle || tiles.all { it.cell?.x == tiles.first().cell?.x }

        tiles.forEach {
            boardService.layTile(board.id ?: "", it, it.cell?.x ?: -1, it.cell?.y ?: -1)
        }

        val cell = tiles.first().cell ?: throw IllegalMoveException("The tile is not laid on the board")

        try {
            if (isHorizontal) {
                val newWord = checkHorizontalAsync(board, tiles, cell).await()
                if (newWord != null)
                    words += newWord
            }
            if (isVertical) {
                val newWord = checkVerticalAsync(board, tiles, cell).await()
                if (newWord != null)
                    words += newWord
            }
        } catch(ex: IllegalMoveException) {
            tiles.forEach {
                boardService.removeTile(board.id ?: "", it.cell?.x ?: -1, it.cell?.y ?: -1)
            }
            throw ex
        }

        return words
    }

    /**
     * Check the [Word] when the [Tile]s are laid horizontal.
     * @param board [Board] to be checked.
     * @param tiles [Tile]s to be checked.
     * @param cell [Cell] to start from.
     * @return Async [WordRepresentation]
     * @author Nick Van Osta
     */
    private fun checkHorizontalAsync(board: Board, tiles: List<Tile>, cell: Cell) = GlobalScope.async {
        val left = GlobalScope.async { floodLeft(board, tiles, cell.x, cell.y, leaf = false) }
        val right = GlobalScope.async { floodRight(board, tiles, cell.x + 1, cell.y, leaf = false) }
        val word = left.await() + right.await()
        return@async if (word.size > 1) checkWord(word, tiles, false) else null
    }

    /**
     * Check the [Word] when the [Tile]s are laid vertical.
     * @param board [Board] to be checked.
     * @param tiles [Tile]s to be checked.
     * @param cell [Cell] to start from.
     * @return Async [WordRepresentation]
     * @author Nick Van Osta
     */
    private fun checkVerticalAsync(board: Board, tiles: List<Tile>, cell: Cell) = GlobalScope.async {
        val up = GlobalScope.async { floodUp(board, tiles, cell.x, cell.y, leaf = false) }
        val down = GlobalScope.async { floodDown(board, tiles, cell.x, cell.y + 1, leaf = false) }
        val word = up.await() + down.await()
        return@async if (word.size > 1) checkWord(word, tiles, true) else null
    }

    /**
     * Left facing "flooding" algorithm to determine all neighbouring [Tile]s to the west.
     * @param board [Board] to be checked.
     * @param tiles [Tile]s to be checked.
     * @param x Current x coordinate.
     * @param y Current y coordinate.
     * @param acc Accumulation of all found [Tile]s.
     * @param leaf Whether this is not part of the main laid word.
     * @return List of found [Tile]s.
     * @author Nick Van Osta
     */
    private tailrec suspend fun floodLeft(board: Board, tiles: List<Tile>, x: Int, y: Int, acc: MutableList<Tile> = mutableListOf(), leaf: Boolean = true): List<Tile> {
        val tile = getCellOnBoard(board, x, y)?.tile ?: return acc
        acc.add(0, tile)

        if (tiles.contains(tile) && !leaf) {
            val up = GlobalScope.async { floodUp(board, tiles, x, y - 1) }
            val down = GlobalScope.async { floodDown(board, tiles, x, y + 1) }
            val wordTiles = up.await() + tile + down.await()
            if (wordTiles.size > 1)
                checkWord(wordTiles, tiles, true)
        }

        return floodLeft(board, tiles, x - 1, y, acc)
    }

    /**
     * Right facing "flooding" algorithm to determine all neighbouring [Tile]s to the east.
     * @param board [Board] to be checked.
     * @param tiles [Tile]s to be checked.
     * @param x Current x coordinate.
     * @param y Current y coordinate.
     * @param acc Accumulation of all found [Tile]s.
     * @param leaf Whether this is not part of the main laid word.
     * @return List of found [Tile]s.
     * @author Nick Van Osta
     */
    private tailrec suspend fun floodRight(board: Board, tiles: List<Tile>, x: Int, y: Int, acc: MutableList<Tile> = mutableListOf(), leaf: Boolean = true): List<Tile> {
        val tile = getCellOnBoard(board, x, y)?.tile ?: return acc
        acc.add(tile)

        if (tiles.contains(tile) && !leaf) {
            val up = GlobalScope.async { floodUp(board, tiles, x, y - 1) }
            val down = GlobalScope.async { floodDown(board, tiles, x, y + 1) }
            val wordTiles = up.await() + tile + down.await()
            if (wordTiles.size > 1)
                checkWord(wordTiles, tiles, true)
        }

        return floodRight(board, tiles, x + 1, y, acc)
    }

    /**
     * Downward "flooding" algorithm to determine all neighbouring [Tile]s to the south.
     * @param board [Board] to be checked.
     * @param tiles [Tile]s to be checked.
     * @param x Current x coordinate.
     * @param y Current y coordinate.
     * @param acc Accumulation of all found [Tile]s.
     * @param leaf Whether this is not part of the main laid word.
     * @return List of found [Tile]s.
     * @author Nick Van Osta
     */
    private tailrec suspend fun floodUp(board: Board, tiles: List<Tile>, x: Int, y: Int, acc: MutableList<Tile> = mutableListOf(), leaf: Boolean = true): List<Tile> {
        val tile = getCellOnBoard(board, x, y)?.tile ?: return acc
        acc.add(0, tile)

        if (tiles.contains(tile) && !leaf) {
            val left = GlobalScope.async { floodLeft(board, tiles, x - 1, y) }
            val right = GlobalScope.async { floodRight(board, tiles, x + 1, y) }
            val wordTiles = left.await() + tile + right.await()
            if (wordTiles.size > 1)
                checkWord(wordTiles, tiles, false)
        }

        return floodUp(board, tiles, x, y - 1, acc)
    }

    /**
     * Upward "flooding" algorithm to determine all neighbouring [Tile]s to the north.
     * @param board [Board] to be checked.
     * @param tiles [Tile]s to be checked.
     * @param x Current x coordinate.
     * @param y Current y coordinate.
     * @param acc Accumulation of all found [Tile]s.
     * @param leaf Whether this is not part of the main laid word.
     * @return List of found [Tile]s.
     * @author Nick Van Osta
     */
    private tailrec suspend fun floodDown(board: Board, tiles: List<Tile>, x: Int, y: Int, acc: MutableList<Tile> = mutableListOf(), leaf: Boolean = true): List<Tile> {
        val tile = getCellOnBoard(board, x, y)?.tile ?: return acc
        acc.add(tile)

        if (tiles.contains(tile) && !leaf) {
            val left = GlobalScope.async { floodLeft(board, tiles, x - 1, y) }
            val right = GlobalScope.async { floodRight(board, tiles, x + 1, y) }
            val wordTiles = left.await() + tile + right.await()
            if (wordTiles.size > 1)
                checkWord(wordTiles, tiles, false)
        }

        return floodDown(board, tiles, x, y + 1, acc)
    }

    /**
     * Check if a [Word] is valid, score it and create its string representation.
     * @param tiles [Tile]s to be checked.
     * @param laidTiles [Tile]s laid by the [User].
     * @param vertical Whether the [Word] is laid vertical or not.
     * @return A [WordRepresentation] for the formed [Word].
     * @author Nick Van Osta
     */
    private fun checkWord(tiles: List<Tile>, laidTiles: List<Tile>, vertical: Boolean): WordRepresentation {
        var score = 0
        var wordMultiplier = 1
        val word = tiles.map { it.letter.char }.joinToString("")
        var representation = ""
        val foundWord = wordService.findWordByLanguage(word, tiles.first().letter.language)

        // Stringify word, coords, score
        // e.g.: (W)OrD A1 +1
        val tempLaidTiles = laidTiles.toMutableList()
        var i = 0
        word.forEach { c ->
            val tile = tempLaidTiles.firstOrNull { it.letter.char == c }
            val char = if (c == '.') foundWord.word[i].toLowerCase() else c
            representation += if (tile == null) "($char)" else char
            i++
        }

        // remove conflicting braces
        representation = representation.replace(")(", "")

        // Score
        val bonus = if ((laidTiles.size) >= maxTilesInLetterRack) 50 else 0
        tiles.forEach {
            var letterMultiplier = 1

            // use multiplier
            if (it.cell?.used != true) {
                when (it.cell?.multiplier) {
                    Multiplier.X2_WORD -> wordMultiplier *= 2
                    Multiplier.X3_WORD -> wordMultiplier *= 3
                    Multiplier.X2_LETTER -> letterMultiplier *= 2
                    Multiplier.X3_LETTER -> letterMultiplier *= 3
                    else -> letterMultiplier *= 1
                }
                it.cell?.used = true
            }

            score += letterMultiplier * it.letter.score
        }

        val firstTile = tiles.sortedBy { it.cell?.y }.sortedBy { it.cell?.x }.first().cell!!
        return WordRepresentation(representation, score * wordMultiplier + bonus, firstTile.y + 1, 'A' + firstTile.x, vertical)
    }

    /**
     * Check if the laid [Tile]s are all connected to eachother and the rest of the [Board].
     * @param board [Board] to check.
     * @param tiles [Tile]s to check.
     * @author Nick Van Osta
     */
    private fun checkIfConnected(board: Board, tiles: List<Tile>) {
        var anyConnectedToBoard: Boolean = !board.cells.any { it.tile != null }
        tiles.forEach {
            val cell = it.cell ?: throw IllegalMoveException("The tile is not laid on the board")
            val isConnectedToTilesOnBoard = getCellOnBoard(board, cell.x, cell.y)?.tile == null &&
                    (getCellOnBoard(board, cell.x + 1, cell.y)?.tile != null ||
                            getCellOnBoard(board, cell.x - 1, cell.y)?.tile != null ||
                            getCellOnBoard(board, cell.x, cell.y + 1)?.tile != null ||
                            getCellOnBoard(board, cell.x, cell.y - 1)?.tile != null)
            val isConnectedToOthers = tiles.any { tile -> tile.cell?.x == cell.x + 1 } ||
                    tiles.any { t -> t.cell?.x == cell.x - 1 } ||
                    tiles.any { t -> t.cell?.y == cell.y + 1 } ||
                    tiles.any { t -> t.cell?.y == cell.y - 1 }

            anyConnectedToBoard = anyConnectedToBoard || isConnectedToTilesOnBoard

            if (!isConnectedToOthers && !isConnectedToTilesOnBoard) throw IllegalMoveException("The tiles are not connected to eachother or the tiles on the board")
        }
        if (!anyConnectedToBoard) throw IllegalMoveException("The tiles are not connected to the tiles on the board")
    }

    /**
     * Get the [Cell] on a certain coordinate from a given [Board].
     * @param board [Board] to fetch from.
     * @param x x coordinate.
     * @param y y coordinate.
     * @author Nick Van Osta
     */
    private fun getCellOnBoard(board: Board, x: Int, y: Int): Cell? {
        if (x < 0 || y < 0 || x >= board.width || y >= board.height) return null

        return board.cells[y * board.width + x]
    }

    /**
     * Check if the center [Cell] has a [Tile] placed on it.
     * @param board [Board] to check.
     * @param tiles [Tile]s to check.
     * @author Nick Van Osta
     */
    private fun checkCenterTile(board: Board, tiles: List<Tile>) {
        val centerCell = board.cells[board.cells.size / 2]
        val centerFilled = centerCell.tile != null || tiles.any { it.cell == centerCell }

        if (!centerFilled) throw IllegalMoveException("The center cell on the board should be filled")
    }

    /**
     * Check if the laid [Tile]s are in line with eachother.
     * @param tiles [Tile]s to check.
     * @author Nick Van Osta
     */
    private fun checkLettersInLine(tiles: List<Tile>) {
        val isVertical = tiles.all { it.cell?.y == tiles.first().cell?.y }
        val isHorizontal = tiles.all { it.cell?.x == tiles.first().cell?.x }

        if (!isVertical && !isHorizontal) throw IllegalMoveException("The tiles are not laid down in a straight line")
    }
}
