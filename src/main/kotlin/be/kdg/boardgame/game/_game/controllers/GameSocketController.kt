package be.kdg.boardgame.game._game.controllers

import be.kdg.boardgame.authentication.dto.UserDTO
import be.kdg.boardgame.authentication.services.UserService
import be.kdg.boardgame.game._game.dto.GameDTO
import be.kdg.boardgame.game._game.dto.PlaceTilesRequestDTO
import be.kdg.boardgame.game._game.dto.ScoreDTO
import be.kdg.boardgame.game._game.service.GameService
import be.kdg.boardgame.game._game.service.ScoreService
import be.kdg.boardgame.notifications.service.NotificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller


@Controller
class GameSocketController {

    @Autowired
    private lateinit var gameService: GameService
    @Autowired
    private lateinit var template: SimpMessagingTemplate
    @Autowired
    private lateinit var scoreService: ScoreService
    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var notifService: NotificationService


    /**
     * websocket version of @fun placeTiles
     * just passes the laidTiles to the other users
     * after @fun: placeTiles sent a success response to the one who laid the tiles
     * in the frontend of the app. used to update the boards of all players in a game dynamically.
     *
     *  @return sends scores from all the players in a given game
     *  and the updated board to a websocket topic
     *
     *  sends notifications:
     *  - to players who's turn is next
     *  - about the turns of others to all other players
     *  - when the game is finished
     *
     * (IDE grays out the name of this method but it will still be used by the Websocket)
     */
    @MessageMapping("/games/{gameId}/tiles/send")
    fun boardStatusUpdate(requestBody: PlaceTilesRequestDTO) {
        val gameId = requestBody.gameId
        val game = gameService.getGameById(gameId)
        val scores = scoreService.getScoresByGame(gameId)
        val notifTitle = "Finished turn"
        val message = "Player ${requestBody.username} has played the word ${requestBody.tiles.map { it.char }}"
        val usernames = userService.findAllUsersByGame(game).map { it.username }.toMutableList()
        val users = userService.findAllUsersByGame(game)

        // send tiles to update board
        requestBody.tiles.forEach {
            println("these tiles have entered socket topic: ${it.char} from game $gameId")
        }

        val gdto = GameDTO(game.id,
                UserDTO(game.creator.username, game.creator.email, game.creator.avatar, game.creator.accountIsActivated),
                game.title,
                users.map { UserDTO(it.username, it.email, it.avatar, it.accountIsActivated) },
                scores.map { ScoreDTO(it.game.id, it.user.username, it.score) },
                game.board.toString(),
                game.currentlyPlaying,
                game.isOver,
                game.hasStarted,
                game.winner)

        this.template.convertAndSend("/games/receive/$gameId", gdto)

        if (gdto.over) {
            users.forEach {
                this.template.convertAndSend("/games/end/receive/${it.username}", gdto)
            }

            notifService.createNotification(
                    usernames,
                    gameId,
                    "Game Over!",
                    "The winner of ${gdto.title} is ${gdto.winner}!",
                    mutableListOf())
        }

        // todo: notification settings
        // send notif about played turn to all other players in game
        println(message)
        usernames.forEach { println("sending notif to: $it") }
        notifService.createNotification(
                usernames,
                gameId,
                notifTitle,
                message,
                excludeUsers = mutableListOf(requestBody.username)
        )

        // send notif to the player who's turn it is
        val nextPlayerUsername = usernames[game.currentlyPlaying]
        println("NEXT PLAYER IS $nextPlayerUsername")
        notifService.createNotification(
                mutableListOf(nextPlayerUsername),
                gameId,
                "It's your turn",
                "You can make a word in game ${game.title}",
                excludeUsers = mutableListOf()
        )
    }

}
