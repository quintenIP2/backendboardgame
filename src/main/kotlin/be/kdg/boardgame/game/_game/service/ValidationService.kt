package be.kdg.boardgame.game._game.service

import be.kdg.boardgame.game._board.models.Board
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._game.dto.PlayedTurnDTO

interface ValidationService {
    /**
     * Validates the laid [Tile]s for given [Board].
     * @param board: [Board] to lay [Tile]s on.
     * @param tiles: List of [Tile]s to be laid on the [Board].
     * @return Whether the [Tile]s are validly placed on the [Board].
     * @author Nick Van Osta
     */
    fun validateLaidDownTiles(board: Board, tiles: List<Tile>): Boolean

    /**
     * Scores the laid [Tile]s for a given [Board].
     * @param board: [Board] to lay [Tile]s on.
     * @param tiles: List of [Tile]s to be laid on the [Board].
     * @return The played [Turn] with the formed [Word]s and [Score]s.
     * @author Nick Van Osta
     */
    fun scoreLaidDownTiles(board: Board, tiles: List<Tile>): PlayedTurnDTO
}
