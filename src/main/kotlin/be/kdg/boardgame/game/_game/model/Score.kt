package be.kdg.boardgame.game._game.model

import be.kdg.boardgame.authentication.models.User
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
data class Score(
        @ManyToOne
        var game: Game,
        @ManyToOne
        var user: User,
        @Column
        var score: Int = 0,

        @Id
        @Column
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "uuid2")
        var id: String = ""
)
