package be.kdg.boardgame.game._game.service

import be.kdg.boardgame.game._board.exceptions.IllegalMoveException
import be.kdg.boardgame.game._game.model.Word
import be.kdg.boardgame.game._game.persistence.WordRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class WordServiceImpl
    : WordService {

    @Autowired
    private lateinit var wordRepository: WordRepository

    override fun findWordByLanguage(word: String, language: String): Word {
        val foundWord: Word?
        foundWord = if (word.contains('.')) {
            // prepend '^' and append '$' when necessary
            val regex = (if (!word.startsWith('^')) "^" else "") +
                    word +
                    (if (!word.endsWith('$')) "$" else "")
            wordRepository.findByWordRegexAndLanguage(regex, language).firstOrNull()
        } else {
            wordRepository.findByWordAndLanguage(word, language)
        }

        return foundWord ?: throw IllegalMoveException("Invalid word '$word' for language '$language' laid down")
    }
}
