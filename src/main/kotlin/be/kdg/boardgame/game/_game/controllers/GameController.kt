package be.kdg.boardgame.game._game.controllers

import be.kdg.boardgame.authentication.dto.UserDTO
import be.kdg.boardgame.authentication.services.UserService
import be.kdg.boardgame.game._game.dto.*
import be.kdg.boardgame.game._game.service.GameService
import be.kdg.boardgame.game._game.service.TurnService
import be.kdg.boardgame.notifications.service.NotificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/")
class GameController {
    @Autowired
    private lateinit var gameService: GameService
    @Autowired
    private lateinit var turnService: TurnService
    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var notifService: NotificationService


    @GetMapping("/games/{id}", produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
    @ResponseBody
    fun getGameById(player: Authentication, @PathVariable id: String): ResponseEntity<GameDTO> {
        if (player.isAuthenticated) {
            val game = gameService.getGameById(id)
            val users = userService.findAllUsersByGame(game)
            return ResponseEntity.ok(GameDTO(
                    game.id,
                    UserDTO(game.creator.username, game.creator.email, game.creator.avatar),
                    game.title,
                    users.map { UserDTO(it.username, it.email, it.avatar) }.toMutableList(),
                    game.scores.map { ScoreDTO(it.game.id, it.user.username, it.score) },
                    game.board.toString(),
                    game.currentlyPlaying,
                    game.isOver,
                    game.hasStarted,
                    game.winner
            ))
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null)
    }

    @PostMapping("games/{creator}/{gameName}")
    fun joinGame(
            authentication: Authentication,
            @PathVariable creator: String,
            @PathVariable gameName: String
    ): ResponseEntity<GameDTO> {
        val game = gameService.addPlayer(creator, gameName, authentication.name)
        return ResponseEntity.ok(GameDTO(
                game.id,
                UserDTO(game.creator.username, game.creator.email, game.creator.avatar, game.creator.accountIsActivated),
                game.title,
                game.users.map { UserDTO(it.username, it.email, it.avatar, it.accountIsActivated) },
                game.scores.map { ScoreDTO(it.game.id, it.user.username, it.score) },
                game.board.toString(),
                game.currentlyPlaying,
                game.isOver,
                game.hasStarted
        ))
    }

    // gameplay
    @PostMapping("games/{gameId}/tiles")
    fun placeTiles(authentication: Authentication,
                   @PathVariable gameId: String,
                   @RequestBody requestBody: PlaceTilesRequestDTO): ResponseEntity<PlayedTurnDTO> {
        val result = gameService.playTiles(authentication.name, gameId, requestBody.tiles)
        return ResponseEntity.ok(result)
    }

    /**
     * If all players have chosen either true or false
     * A signal will be sent to the gameoverview (socket) to put the game in the started category
     */
    @PostMapping("/games/{gameId}/invitations")
    fun handleInvitationResponses(authentication: Authentication,
                                  @PathVariable gameId: String,
                                  @RequestBody inv: AcceptInvitationDTO): ResponseEntity<Any> {
        if (authentication.isAuthenticated) {
            val result = gameService.handleInvitationResponses(authentication.name, gameId, inv.hasAccepted)

            // send back game to user who accepted to update overview

            return ResponseEntity.ok(InvitationDTO(result, authentication.name))
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null)
    }

    // new game
    @PostMapping("/games")
    fun createGame(authentication: Authentication,
                   @RequestBody requestBody: NewGameRequestDTO): ResponseEntity<GameDTO> {
        if (authentication.isAuthenticated) {
            val game = gameService.createGame(
                    authentication.name,
                    requestBody.title,
                    requestBody.language,
                    requestBody.players)

            // send notif to invited players about new game
            notifService.createNotification(
                    game.users.map { it.username }.toMutableList(),
                    game.id,
                    "Game invitation",
                    "${game.creator.username} has invited you to a game",
                    mutableListOf(game.creator.username))

            return ResponseEntity.ok(GameDTO(
                    game.id,
                    UserDTO(game.creator.username, game.creator.email, game.creator.avatar),
                    game.title,
                    game.users.map { UserDTO(it.username, it.email, it.avatar) }.toMutableList(),
                    game.scores.map { ScoreDTO(it.game.id, it.user.username, it.score) },
                    game.board.toString(),
                    game.currentlyPlaying,
                    game.isOver,
                    game.hasStarted
            ))
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null)
    }

    // overview
    @DeleteMapping("/games/{id}")
    fun deleteGame(authentication: Authentication, @PathVariable id: String): ResponseEntity<Boolean> {
        if (authentication.isAuthenticated) {
            return ResponseEntity.ok(gameService.deleteGame(id, authentication.name))
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null)
    }

    @PostMapping("games/{gameId}")
    fun startGame(
            authentication: Authentication,
            @PathVariable gameId: String
    ): ResponseEntity<GameDTO> {
        val game = gameService.startGame(authentication.name, gameId)
        return ResponseEntity.ok(GameDTO(
                game.id,
                UserDTO(
                        game.creator.username,
                        game.creator.email,
                        game.creator.avatar,
                        game.creator.accountIsActivated
                ),
                game.title,
                game.users.map {
                    UserDTO(
                            it.username,
                            it.email,
                            it.avatar,
                            it.accountIsActivated
                    )
                },
                game.scores.map { ScoreDTO(it.game.id, it.user.username, it.score) },
                game.board.toString(),
                game.currentlyPlaying,
                game.isOver,
                game.hasStarted
        ))
    }

    // replays
    @GetMapping("games/{gameId}/turns")
    fun getAllTurns(auth: Authentication, @PathVariable gameId: String): ResponseEntity<List<ReplayTurnDTO>> {
        if (auth.isAuthenticated) {
            val turns = turnService.getAllTurnsByGameId(gameId)

            val turnsMapped = turns.map { t ->
                ReplayTurnDTO(
                        t.game.id,
                        t.turnCounter,
                        t.board,
                        t.player.username,
                        t.score,
                        t.racks,
                        t.game.users.map { u -> UserDTO(u.username, u.email, u.avatar) }.toMutableList()
                )
            }
            return ResponseEntity.ok(turnsMapped)
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null)
    }

}
