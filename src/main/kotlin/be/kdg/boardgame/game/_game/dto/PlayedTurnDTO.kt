package be.kdg.boardgame.game._game.dto

data class PlayedTurnDTO (
        val words: List<WordScoreDTO>,
        val representation: String,
        val board: String,
        var over: Boolean = false
)
