package be.kdg.boardgame.game._game.exceptions

import java.util.*

data class GameErrorDetails (
        val timestamp: Date,
        val message: String,
        val details: String
)
