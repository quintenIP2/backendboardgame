package be.kdg.boardgame.game._game.dto

data class InvitationDTO(val hasAccepted: Boolean, val user: String)
