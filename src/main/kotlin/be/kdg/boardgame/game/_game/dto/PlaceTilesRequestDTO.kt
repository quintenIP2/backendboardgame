package be.kdg.boardgame.game._game.dto

data class PlaceTilesRequestDTO(
        val tiles: List<LaidDownTileDTO>,
        val username: String = "",
        val gameId: String = ""
)
