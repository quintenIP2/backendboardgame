package be.kdg.boardgame.game._game.model

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._board.models.Board
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._letter_rack.models.LetterRack
import org.hibernate.annotations.GenericGenerator
import java.util.*
import javax.persistence.*

/**
 * A Scrabble game played by a number of [User]s.
 *
 * @property users the list of players in this game.
 * @property board the [Board] which is played on.
 * @property turns the amount of turns that we're played in a game
 * @property racks the [User]'s [LetterRack]s.
 * @property currentlyPlaying an Int specifying who's turn it is.
 * @property isOver a Boolean specifying if the game is over.
 * @constructor Creates a new game without [User]s and a clean [Board], unless a list of [User]s or a [Board] is given.
 * @author Nick Van Osta & Quinten Spillemaeckers & Jonathan Peers
 */
@Entity
@Table(name = "tbl_game")
data class Game(
        @ManyToOne
        var creator: User = User(),
        @Column
        var title: String = "Scrabble Game",
        @Column
        val language: String = "en",
        @ManyToMany(cascade = [CascadeType.ALL])
        var users: MutableList<User> = mutableListOf(),
        @OneToMany(cascade = [CascadeType.ALL])
        var turns: MutableList<Turn> = mutableListOf(),
        @OneToOne(cascade = [CascadeType.ALL])
        var board: Board? = null,
        @OneToMany(cascade = [CascadeType.ALL])
        var racks: MutableList<LetterRack> = mutableListOf(),
        @OneToMany(cascade = [CascadeType.ALL])
        var scores: List<Score> = mutableListOf(),
        @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        var invitations: MutableList<Invitation> = mutableListOf(),
        @Column
        var currentlyPlaying: Int = 0,
        @Column
        var winner: String = "",
        @Column
        var hasStarted: Boolean = false,
        @Column
        var isOver: Boolean = false,
        @Column
        var skippedTurns: Int = 0,
        @Transient
        var letterBag: Queue<Tile> = ArrayDeque<Tile>()
) {
    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    var id: String = ""

    override fun toString(): String {
        return "Game(creator=$creator, title='$title', winner=$winner, id='$id')"
    }
}
