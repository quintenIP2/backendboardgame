package be.kdg.boardgame.game._game.service

import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._game.dto.LaidDownTileDTO
import be.kdg.boardgame.game._game.dto.PlayedTurnDTO
import be.kdg.boardgame.game._game.model.Game
import org.springframework.stereotype.Service

@Service
interface GameService {
    /**
     * Get [Game] with given id.
     * @param id Id to search for.
     * @return The requested [Game].
     */
    fun getGameById(id: String): Game

    /**
     * Get [Game] with given title for given [User].
     * @param email [User]'s email address.
     * @param title [Game]'s title.
     * @return The requested [Game].
     */
    fun getGameByName(email: String, title: String): Game

    /**
     * Place tiles on the [Board].
     * @param email [User]'s email address.
     * @param gameId [Game]'s id.
     * @param cellsToUpdate list of [Tile]s to lay down on the board.
     * @return The played [Turn].
     * @author Nick Van Osta
     */
    fun playTiles(email: String, gameId: String, cellsToUpdate: List<LaidDownTileDTO>): PlayedTurnDTO

    /**
     * Make given user draw [Tile]s from the letter bag.
     * @param email [User]'s email address.
     * @param gameId [Game]'s id.
     * @param amount Amount of [Tile]s to draw from the letter bag.
     * @return List of drawn [Tile]s.
     * @author Nick Van Osta
     */
    fun drawTiles(email: String, gameId: String, amount: Int): List<Tile>

    /**
     * Add [User] to a [Game] as a player.
     * @param creator Creator's email address.
     * @param gameId [Game]'s id.
     * @param email Email address of [User] to be added.
     * @return The updated [Game].
     * @author Nick Van Osta
     */
    fun addPlayer(creator: String, gameId: String, email: String): Game

    /**
     * Start the [Game].
     * @param email [User]'s email address.
     * @param gameId [Game]'s id.
     * @return The started [Game]
     * @author Nick Van Osta
     */
    fun startGame(email: String, gameId: String): Game

    /**
     * End the [Game].
     * @param gameId [Game]'s id.
     * @return The finished [Game].
     * @author Nick Van Osta
     */
    fun endGame(gameId: String): Game

    /**
     * Create a new [Game].
     * @param creator Creator's email address.
     * @param title [Game]'s title.
     * @param language: [Game]'s language.
     * @param players: List of username's to be added to the [Game]
     */
    fun createGame(creator: String, title: String, language: String, players: MutableList<String>): Game

    /**
     * Find whose turn it is for a certain [Game].
     * @param game [Game] you need the next player of.
     * @return Index of the [User] whose turn it is.
     */
    fun findNextPlayer(game: Game): Int

    /**
     * Find whose turn it is for a certain list of usernames.
     * @param currentlyPlaying Index of the player whose turn it is.
     * @param users List of usernames.
     * @return Index of the [User] whose turn it is.
     */
    fun findNextPlayer(currentlyPlaying: Int, users: MutableList<String>): Int

    /**
     * Delete [Game].
     * @param id [Game]'s id.
     * @param creatorEmail [User]'s email address.
     * @return Whether the operation succeeded.
     */
    fun deleteGame(id: String, creatorEmail: String): Boolean

    /**
     * Handles invitation responses.
     * @param email [User]'s email address.
     * @param gameId [Game]'s id.
     * @param hasAccepted Boolean for whether the [User] has accepted the invitation or not.
     * @return Whether all invitations were accepted yet.
     * @author Jonathan Peers
     */
    fun handleInvitationResponses(email: String, gameId: String, hasAccepted: Boolean): Boolean
}
