package be.kdg.boardgame.game._game.model

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._board.models.Board
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._letter_rack.models.LetterRack
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

/**
 * A turn in a game, used to reform replays of a game.
 *
 * @property game the [Game] this turn was played in.
 * @property turnCounter an identifier to know which turn this was in the game.
 * @property board a String resembling the cells of a [Board] to represent the difference in turns,
 *  represents the history of all turns in a String to decrease storage space needed
 *  will be marshalled to List of Cell to update the board object in relation to frontend
 *
 * @property player the [User] playing this turn.
 * @property racks the [User]'s [LetterRack]s, so we know which [Tile]s they had.
 * @constructor Creates a new turn representation to be replayed later.
 * @author Nick Van Osta & Quinten Spillemaeckers
 */

@Entity
@Table(name = "tbl_turn")
data class Turn(
        @ManyToOne
        val game: Game,
        @Column
        val turnCounter: Int,
        @Column
        var score: Int = 0,
        @Column
        var board: String,
        @ManyToOne
        val player: User,
        @Column
        val racks: String
) {

    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    var id: String? = null

}
