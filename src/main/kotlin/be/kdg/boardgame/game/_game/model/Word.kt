package be.kdg.boardgame.game._game.model

import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
@Table(name = "tbl_word")
data class Word(
        @Column
        val word: String = "",
        @Column
        val language: String = "",

        @Id
        @Column
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "uuid2")
        var id: String? = null
)
