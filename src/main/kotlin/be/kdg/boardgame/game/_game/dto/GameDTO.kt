package be.kdg.boardgame.game._game.dto

import be.kdg.boardgame.authentication.dto.UserDTO

data class GameDTO(
        val id: String? = "",
        val creator: UserDTO = UserDTO("", "", ""),
        val title: String = "",
        val players: List<UserDTO> = emptyList(),
        val scores: List<ScoreDTO> = emptyList(),
        val board: String = "",
        val currentlyPlaying: Int = 0,
        val over: Boolean = false,
        val hasStarted: Boolean = false,
        val winner: String = ""
)
