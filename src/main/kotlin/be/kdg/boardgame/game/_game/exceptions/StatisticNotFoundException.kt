package be.kdg.boardgame.game._game.exceptions

import java.lang.RuntimeException

class StatisticNotFoundException(
        message: String? = "no message",
        cause: Throwable? = null
) : RuntimeException(message, cause)