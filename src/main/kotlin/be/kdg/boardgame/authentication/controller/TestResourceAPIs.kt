package be.kdg.boardgame.authentication.controller

import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin(origins = ["*"], maxAge = 3600)
@RestController
class TestResourceAPIs {

    @GetMapping("/api/test/user")
    fun userAccess(): String {
        return ">>> User Contents!"
    }

    @GetMapping("/api/test/pm")
    fun projectManagementAccess(): String {
        return ">>> Project Management Board"
    }

    @GetMapping("/api/test/admin")
    fun adminAccess(): String {
        return ">>> Admin Contents"
    }
}
