package be.kdg.boardgame.authentication.controller

import be.kdg.boardgame.authentication.dto.SignUpResponse
import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.authentication.dto.UserDTO
import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.authentication.persistence.UserRepository
import be.kdg.boardgame.game._statistics.model.Statistic
import be.kdg.boardgame.game._statistics.persistence.StatisticRepository
import be.kdg.boardgame.authentication.services.UserService
import com.sendgrid.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.*
import javax.validation.Valid
import java.io.IOException
import java.net.URI
import java.util.*


/**
 *  REST API to register players
 *  @property userRepository: UserRepository to store and request players in/from db
 *  @property bCryptPasswordEncoder: PasswordEncoder handles password hashing (The preferred implementation is "bCryptPasswordEncoder")
 *  @author Jonathan Peers & Quinten Spillemaeckers
 */

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping("/api/")
class AuthController {

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var statisticRepository: StatisticRepository

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

    @PostMapping("/register", produces = ["application/json"])
    fun registerUser(@Valid @RequestBody signUpRequest: SignUpForm): ResponseEntity<SignUpResponse> {
        // error responses
        if (userRepository.existsByUsername(signUpRequest.username)) {
            return ResponseEntity(SignUpResponse("Fail -> Username is already taken!"), HttpStatus.BAD_REQUEST)
        }

        if (userRepository.existsByEmail(signUpRequest.email)) {
            return ResponseEntity(SignUpResponse("Fail -> Email is already in use!"), HttpStatus.BAD_REQUEST)
        }

        // Creating user's account
        val user = User(
                signUpRequest.email, bCryptPasswordEncoder.encode(signUpRequest.password),
                signUpRequest.username, userService.convertImageToString("/pictures/player-logo.png") , true)
        userRepository.save(user)
        statisticRepository.save(Statistic(user))
        return ResponseEntity(SignUpResponse("User registered successfully!"), HttpStatus.OK)
    }

    @GetMapping("/user/activate/{email}", produces = ["application/json"])
    fun activateAccountByEmail(@PathVariable email: String): ResponseEntity<Any> {
        //probably better with only one query to db
        if (!userRepository.existsByEmail(email)) {
            return ResponseEntity("Unauthorized activation request. Please check email address.", HttpStatus.BAD_REQUEST)
        }
        val user = userRepository.findByEmail(email).get()
        user.accountIsActivated = true
        userRepository.save(user)

        // send email
        sendActivationEmail(user)

        //redirect to home
        val headers = HttpHeaders()
        headers.location = URI("http://localhost:8080/login")
        return ResponseEntity("Email activation completed!", headers, HttpStatus.ACCEPTED)
    }

    @GetMapping("/user")
    fun getUser(auth: Authentication): SignUpForm {
        val user = userRepository.findByEmail(auth.name).get()
        println(SignUpForm(user.username, user.email, user.password))
        return SignUpForm(user.username, user.email, user.password)
    }

    @PutMapping("/user/username")
    fun updateUsername(@Valid @RequestBody user: SignUpForm, auth: Authentication): ResponseEntity<SignUpResponse?> {
        val userToUpdate: Optional<User> = userRepository.findByEmail(auth.name)
        val newUser: User = userToUpdate.unwrap()
        newUser.username = user.username
        newUser.password = userToUpdate.get().password
        userRepository.save(newUser)
        println("Username updated")
        return ResponseEntity(SignUpResponse("Username updated"), HttpStatus.OK)
    }

    @PutMapping("/user/password")
    fun updatePassword(@Valid @RequestBody user: SignUpForm, auth: Authentication): ResponseEntity<SignUpResponse?> {
        val userToUpdate: Optional<User> = userRepository.findByEmail(auth.name)
        val newUser: User = userToUpdate.unwrap()
        newUser.username = userToUpdate.get().username
        newUser.password = bCryptPasswordEncoder.encode(user.password)
        userRepository.save(newUser)
        println("Password updated")
        return ResponseEntity(SignUpResponse("Password updated"), HttpStatus.OK)
    }

    //Optional<User> --> User
    private fun <User> Optional<User>.unwrap(): User = orElse(null)

    @GetMapping("/user/", produces = ["application/json"])
    fun getUserDetails(
            authentication: Authentication
    ): ResponseEntity<Any> {
        val user = userService.findUserByEmail(authentication.name)
        val userDto = UserDTO(user.username, user.email, user.avatar, user.accountIsActivated)
        return ResponseEntity(userDto, HttpStatus.OK)

    }


    @GetMapping("/user/{email}", produces = ["application/json"])
    fun getUserDetails(@PathVariable email: String): ResponseEntity<Any> {
        if (!userRepository.existsByEmail(email)) {
            return ResponseEntity("Fail -> Email is not found!", HttpStatus.BAD_REQUEST)
        }

        val user = userRepository.findByEmail(email).get()
        val userDto = UserDTO(user.username, user.email, user.avatar, user.accountIsActivated)
        return ResponseEntity(userDto, HttpStatus.OK)
    }



    private fun sendActivationEmail(user: User) {
        val from = Email("quintenspillemaeckers@gmail.com")
        val button = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "  <tr>\n" +
                "    <td>\n" +
                "      <div>\n" +
                "        <!--[if mso]>\n" +
                "          <v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"http://litmus.com\" style=\"height:36px;v-text-anchor:middle;width:150px;\" arcsize=\"5%\" strokecolor=\"#EB7035\" fillcolor=\"#EB7035\">\n" +
                "            <w:anchorlock/>\n" +
                "            <center style=\"color:#ffffff;font-family:Helvetica, Arial,sans-serif;font-size:16px;\">I am a button &rarr;</center>\n" +
                "          </v:roundrect>\n" +
                "        <![endif]-->\n" +
                "        <a href=\"http://localhost:4200/login" +
                "login\" style=\"background-color:#EB7035;border:1px solid #EB7035;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:150px;-webkit-text-size-adjust:none;mso-hide:all;\">Activate</a>\n" +
                "      </div>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "</table>"

        val content = Content("text/html", button)
        val subject = "Your account is ready to be activated\n"
        val to = Email(user.email)
        val mail = Mail(from, subject, to, content)
        val sendGrid = SendGrid("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")
        val request = Request()
        try {
            request.method = Method.POST
            request.endpoint = "mail/send"
            request.body = mail.build()
            val response = sendGrid.api(request)
            System.out.println(response.statusCode)
            System.out.println(response.body)
            System.out.println(response.headers)
        } catch (ex: IOException) {
            throw ex
        }
    }


}
