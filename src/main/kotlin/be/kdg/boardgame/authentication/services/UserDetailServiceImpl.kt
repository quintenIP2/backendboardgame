package be.kdg.boardgame.authentication.services

import be.kdg.boardgame.authentication.persistence.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserDetailServiceImpl : UserDetailsService {

    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(email: String): UserDetails {
        val user = userRepository
                .findByEmail(email)
                .orElseThrow { UsernameNotFoundException("User Not Found with email : $email") }

        val authorities = ArrayList<GrantedAuthority>()

        return User(user.email, user.password, authorities)
    }
}
