package be.kdg.boardgame.authentication.services

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.authentication.persistence.UserRepository
import be.kdg.boardgame.game._game.model.Game
import org.apache.commons.io.FileUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileOutputStream
import java.util.*

@Service
class UserServiceImpl : UserService {

    @Autowired
    lateinit var userRepository: UserRepository

    override fun findRandomRow(): User {
        return userRepository
                .findRandomRow()
                .orElseThrow { UsernameNotFoundException("Did Not Find a random user") }
    }

    override fun save(creatorUser: User): User {
        return userRepository
                .save(creatorUser)
    }

    @Throws(UsernameNotFoundException::class)
    override fun findByUsername(username: String): User {
        return userRepository
                .findByUsername(username)
                .orElseThrow { UsernameNotFoundException("User Not Found with email : $username") }
    }

    @Throws(UsernameNotFoundException::class)
    override fun findUserByEmail(email: String): be.kdg.boardgame.authentication.models.User {
        return userRepository
                .findByEmail(email)
                .orElseThrow { UsernameNotFoundException("User Not Found with email : $email") }
    }

    override fun findAllUsersByGame(game: Game): MutableList<User> {
        return userRepository.findAllByGamesContaining(game)
    }

    override fun convertImageToString(filePath: String): String {
        val file: File?
        var bytes: ByteArray // image in bytes
        val res = javaClass.getResource(filePath) // the actual image
        val resLoader = javaClass.classLoader.getResource(filePath.trimStart { it == '/' }) // imagepath on server
        println("filepath for classloader: ${filePath.trimStart { it == '/' }}")

        if (res.toString().startsWith("jar:") || resLoader.toString().startsWith("jar:")) {
            println("jar structure, using resloader")
            val input = javaClass.classLoader.getResourceAsStream(filePath.trimStart { it == '/' })
            file = File.createTempFile("tempfile", ".tmp")
            val out = FileOutputStream(file)
            var read: Int
            bytes = ByteArray(1024) // size of each read

            while (input.read(bytes).also { read = it } >= 0)
                out.write(bytes, 0, read)

            out.close() // close writer
            file.deleteOnExit() // delete created tmp file
        } else {
            println("file structure, using resloader")
            file = File(res.toURI())
        }

        bytes = FileUtils.readFileToByteArray(file) // convert file to bytes
        return Base64.getEncoder().encodeToString(bytes) // convert bytes to base64 string
    }
}
