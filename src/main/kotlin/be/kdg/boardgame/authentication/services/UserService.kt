package be.kdg.boardgame.authentication.services

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._game.model.Game

interface UserService {
    fun findUserByEmail(email: String): User
    fun findByUsername(username: String): User
    fun findRandomRow(): User
    fun save(creatorUser: User): User
    fun findAllUsersByGame(game: Game): MutableList<User>
    fun convertImageToString(filePath: String): String
}

