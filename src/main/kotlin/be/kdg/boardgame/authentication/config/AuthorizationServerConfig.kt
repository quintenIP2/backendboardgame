package be.kdg.boardgame.authentication.config

import be.kdg.boardgame.authentication.services.UserDetailServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.provider.token.AccessTokenConverter
import org.springframework.security.oauth2.provider.token.DefaultTokenServices
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter


/**
 *  Generates JWT trough oauth2
 *  The Authentication token (JWT) will be used for future API calls to other REST API's (resources)
 *
 *  authManager: AuthenticationManager handles authentication requests
 */
@Configuration
@CrossOrigin(origins = ["*"])
@EnableAuthorizationServer
class AuthorizationServerConfig(
        val bCryptPasswordEncoder: BCryptPasswordEncoder) : AuthorizationServerConfigurerAdapter() {

    @Value("\${client.id}")
    internal val clientId: String? = null
    @Value("\${client.secret}")
    internal val clientSecret: String? = null
    @Value("\${grant_type_password}")
    internal val grantTypePassword: String? = null
    @Value("\${authorization_code}")
    internal val authorizationCode: String? = null

    internal val refreshToken = "refresh_token"
    internal val implicit = "implicit"
    internal val scopeRead = "read"
    internal val scopeWrite = "write"
    internal val trust = "trust"
    internal val accessTokenValiditySeconds = 1 * 60 * 60
    internal val refreshTokenValiditySeconds = 6 * 60 * 60

    @Autowired
    private val authenticationManager: AuthenticationManager? = null

    @Autowired
    private val defaultTokenServices: DefaultTokenServices? = null

    @Autowired
    private val accessTokenConverter: AccessTokenConverter? = null

    @Autowired
    lateinit var userDetailService: UserDetailServiceImpl

    //possible to move to jdbc, inmemory probably has some downsides in the long run
    @Throws(Exception::class)
    override fun configure(configurer: ClientDetailsServiceConfigurer?) {
        configurer!!
                .inMemory()
                .withClient(clientId)
                .secret(bCryptPasswordEncoder.encode(clientSecret))
                .authorizedGrantTypes(grantTypePassword, authorizationCode, refreshToken, implicit)
                .scopes(scopeRead, scopeWrite, trust)
                .accessTokenValiditySeconds(accessTokenValiditySeconds)
                .refreshTokenValiditySeconds(refreshTokenValiditySeconds)
    }

    @Throws(Exception::class)
    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer?) {
        endpoints!!.tokenServices(defaultTokenServices)
                .authenticationManager(authenticationManager)
                .accessTokenConverter(accessTokenConverter)
                .userDetailsService(userDetailService)
    }

    @Bean
    fun corsFiltering(): FilterRegistrationBean<*> {
        val source = UrlBasedCorsConfigurationSource()
        val config = CorsConfiguration()
        config.allowCredentials = true
        config.addAllowedOrigin("http://localhost:4200")
        config.addAllowedOrigin("https://team-bitconnect-web.herokuapp.com")
        config.addAllowedHeader("*")
        config.addAllowedMethod("*")
        source.registerCorsConfiguration("/**", config)
        val bean = FilterRegistrationBean(CorsFilter(source))
        bean.order = Ordered.HIGHEST_PRECEDENCE
        return bean
    }
}
