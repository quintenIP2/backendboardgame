package be.kdg.boardgame.authentication.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.CrossOrigin


/**
 * The resource server config file which determines who can access the resource api
 * Makes use of Oauth2 library to check the jwt tokens
 */

@Configuration
@CrossOrigin(origins = ["*"])
@EnableResourceServer
class ResourceServerConfig : ResourceServerConfigurerAdapter() {

    @Autowired
    private val tokenServices: ResourceServerTokenServices? = null

    @Value("\${resource.id}")
    private val resourceId: String? = null

    override fun configure(resources: ResourceServerSecurityConfigurer?) {
        resources!!.resourceId(resourceId).tokenServices(tokenServices)
    }

    @Bean
    fun oAuth2AccessDeniedHandle(): OAuth2AccessDeniedHandler {
        return OAuth2AccessDeniedHandler()
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.requestMatchers()
                .and()
                .cors() // added this
                .and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/register").permitAll()
                .antMatchers("/api/user/{email}").authenticated()
                .antMatchers("/api/**").authenticated()
    }
}
