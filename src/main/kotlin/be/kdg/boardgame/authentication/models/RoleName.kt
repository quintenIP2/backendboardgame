package be.kdg.boardgame.authentication.models

enum class RoleName(role: String) {
    ROLE_USER("user")
}
