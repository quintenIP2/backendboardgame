package be.kdg.boardgame.authentication.models


import org.hibernate.annotations.NaturalId
import javax.persistence.*

@Entity
@Table(name = "tbl_role")
data class Role(@Id
                @GeneratedValue(strategy = GenerationType.IDENTITY)
                private val id: Long? = null,
                @NaturalId
                @Column(length = 60)
                var name: String = RoleName.ROLE_USER.toString())
