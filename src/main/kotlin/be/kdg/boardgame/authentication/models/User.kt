package be.kdg.boardgame.authentication.models

import be.kdg.boardgame.game._chat.models.ChatMessage
import be.kdg.boardgame.game._letter_rack.models.LetterRack
import be.kdg.boardgame.game._settings.model.models.PlayerSettings
import be.kdg.boardgame.game._statistics.model.Statistic
import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._game.model.Score
import be.kdg.boardgame.game._game.model.Turn
import org.hibernate.annotations.GenericGenerator
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.*

@Entity
@Table(name = "tbl_user")
data class User(

        @Column(name = "email")
        @Email(message = "*Please provide a valid Email")
        @NotBlank(message = "*Please provide an email")
        var email: String = "",

        @Column
        @Pattern(regexp = "(?=^.{6,255}\$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])" +
                "|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])" +
                "(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)" +
                "(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*")
        @NotBlank(message = "*Please provide your password")
        @NotNull(message = "*Password is required")
        var password: String = "",

        @Column
        @Length(min = 5, message = "*Your Username must have at least 5 characters")
        @NotBlank(message = "*Please provide your Username")
        @NotNull(message = "*Username is required")
        var username: String = "",

        @Column(length = 35000)
        @Length(max = 40000)
        @Lob
        var avatar: String = "",

        @Column
        var accountIsActivated: Boolean = false,

        @ManyToMany
        var friends: MutableSet<User> = mutableSetOf(),

        @OneToMany
        val turns: MutableList<Turn> = mutableListOf(),

        @ManyToMany
        val games: MutableList<Game> = mutableListOf(),

        @OneToMany
        val scores: MutableList<Score> = mutableListOf(),

        @OneToMany
        val chatMessages: List<ChatMessage> = emptyList(),

        @OneToOne
        var statistic: Statistic? = null,

        @OneToOne
        var settings: PlayerSettings? = null,

        @OneToMany
        val letterRacks: List<LetterRack> = emptyList()
) {
    @Id
    @Column
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    var id: String? = null

    override fun toString(): String {
        return "User(email='$email', username='$username', id=$id)"
    }
}

