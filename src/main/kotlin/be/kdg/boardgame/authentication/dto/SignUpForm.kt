package be.kdg.boardgame.authentication.dto

import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class SignUpForm(@NotBlank
                      @Size(min = 3, max = 50)
                      var username: String = "",

                      @NotBlank
                      @Size(max = 60)
                      @Email
                      var email: String = "",

                      @NotBlank
                      @Pattern(regexp = "(?=^.{6,255}\$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])" +
                              "|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])" +
                              "(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)" +
                              "(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*")
                      @Size(min = 6, max = 40)
                      var password: String = "")
