package be.kdg.boardgame.authentication.dto

import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._game.model.Score

data class UserDTO(val username: String = "",
                   val email: String = "",
                   val avatar: String = "",
                   val activated: Boolean = false
)
