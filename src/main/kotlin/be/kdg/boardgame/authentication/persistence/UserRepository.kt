package be.kdg.boardgame.authentication.persistence


import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._game.model.Game
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*


@Repository
interface UserRepository : JpaRepository<User, Long> {
    //todo: if you want to switch databases change this to the correct dialect
//    @Query(value = "SELECT TOP 1 * FROM tbl_user ORDER BY NEWID()", nativeQuery = true) // sql server
    @Query(value = "SELECT * FROM tbl_user ORDER BY RAND() LIMIT  1", nativeQuery = true) // h2
    fun findRandomRow(): Optional<User>

    fun findByUsername(username: String?): Optional<User>
    fun existsByUsername(username: String?): Boolean
    fun existsByEmail(email: String?): Boolean
    fun findByEmail(email: String?): Optional<User>
    fun findAllByGamesContaining(game: Game): MutableList<User>
}
