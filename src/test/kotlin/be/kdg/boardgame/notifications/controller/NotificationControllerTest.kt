package be.kdg.boardgame.notifications.controller

import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.authentication.services.UserService
import be.kdg.boardgame.game._game.dto.NewGameRequestDTO
import be.kdg.boardgame.notifications.models.Notification
import be.kdg.boardgame.notifications.service.NotificationService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.oauth2.common.util.JacksonJsonParser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.util.LinkedMultiValueMap
import java.time.LocalDateTime
import javax.transaction.Transactional


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class NotificationControllerTest {
    @Autowired
    lateinit var mockMvc: MockMvc
    @Autowired
    private lateinit var mapper: ObjectMapper
    @Autowired
    private lateinit var notifService: NotificationService
    @Autowired
    private lateinit var userService: UserService

    private val email: String = "hasfriends@gmail.com"
    private val password: String = "hasfriends@gmail.com"
    private val username = "test"
    private var accessToken: String? = null
    private val gameName = "testGame"
    private val language = "en_US"
    private var gameId = ""
    val jsonParser = JacksonJsonParser()

    @Before
    fun setUp() {
        // register
        register(username, email, password)
        accessToken = getToken(email, password)

        // save mock game
        gameId = createGame(gameName, language, mutableListOf(username, "quinten", "None", "None"))
        val user = userService.findUserByEmail(email)

        // simulate chat history between a bot and the creator of the game
        notifService.save(Notification("Content", "title", gameId, LocalDateTime.now(), user))
        notifService.save(Notification("Content2", "title2", gameId, LocalDateTime.now(), user))
    }

    @Test
    @Transactional
    fun testGetNotifications() {
        val notifs = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/notifications")
                        .header("Authorization", "Bearer $accessToken"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn().response.contentAsString
        println(notifs)
    }


    private fun getToken(email: String, password: String): String {
        val params = LinkedMultiValueMap<String, String>()
        params.add("grant_type", "password")
        params.add("client_id", "devglan-client")
        params.add("username", email)
        params.add("password", password)

        val resultString = mockMvc.perform(MockMvcRequestBuilders.post("/oauth/token", params).params(params)
                .with(SecurityMockMvcRequestPostProcessors.httpBasic("devglan-client", "devglan-secret"))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().response.contentAsString
        return jsonParser.parseMap(resultString)["access_token"].toString()
    }

    private fun register(username: String, email: String, password: String) {
        val body = mapper.writeValueAsString(SignUpForm(username, email, password))
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/register")
                .contentType(MediaType.APPLICATION_JSON).content(body))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    private fun createGame(title: String, lang: String, players: MutableList<String>): String {
//        game = gameService.createGame(email, gameName, language, mutableListOf("Bot", "None", "None"))
        val body = mapper.writeValueAsString(NewGameRequestDTO(title, lang, players))

        val createdGame = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/games")
                .header("Authorization", "Bearer $accessToken")
                .contentType(MediaType.APPLICATION_JSON).content(body))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn().response.contentAsString

        println(jsonParser.parseMap(createdGame)["id"].toString())
        return jsonParser.parseMap(createdGame)["id"].toString()
    }
}
