package be.kdg.boardgame.mapper

import org.apache.commons.io.FileUtils
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.io.File
import java.io.IOException
import java.util.*


@RunWith(SpringRunner::class)
@SpringBootTest
class ImageMapperTest {

    @Before
    fun setUp() {
    }

    private val inputFilePath = "pictures/robot1.png"
    private val outputFilePath = "pictures/robot1_copy.png"

    @Test
    @Throws(IOException::class)
    fun fileToBase64StringConversion() {
        // load file from /src/test/resources

        val classLoader = javaClass.classLoader
        val inputFile = File(classLoader.getResource(inputFilePath)!!.file)

        val fileContent = FileUtils.readFileToByteArray(inputFile)
        val encodedString = Base64.getEncoder().encodeToString(fileContent)

        // create output file
        val outputFile = File(inputFile.parentFile.absolutePath + File.pathSeparator + outputFilePath)

        // decode the string and write to file
        val decodedBytes = Base64.getDecoder().decode(encodedString)
        FileUtils.writeByteArrayToFile(outputFile, decodedBytes)

        assertTrue(FileUtils.contentEquals(inputFile, outputFile))
    }
}
