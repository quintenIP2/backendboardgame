package be.kdg.boardgame.authentication.config

import be.kdg.boardgame.authentication.dto.SignUpForm
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.oauth2.common.util.JacksonJsonParser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.util.LinkedMultiValueMap
import javax.transaction.Transactional


@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["spring.profiles.active=test"])
@AutoConfigureMockMvc
class AuthorizationServerConfigTest {
    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    private val email: String = "test@gmail.com"
    private val username: String = "test"
    private val password: String = "test"

    @Before
    fun setup() {
        val body = objectMapper.writeValueAsString(
                SignUpForm(username, email, password))

        mockMvc.perform(post("/api/register").content(body)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(httpBasic("devglan-client", "devglan-secret")))
    }

    // requires existing user to be passed from database
    @Test
    @Transactional
    fun testExistingUserCanObtainJWT() {
        val params = LinkedMultiValueMap<String, String>()
        params.add("grant_type", "password")
        params.add("client_id", "devglan-client")
        params.add("username", email)
        params.add("password", password)

        mockMvc.perform(post("/oauth/token", params).params(params)
                .with(httpBasic("devglan-client", "devglan-secret"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andReturn()
    }

    @Test
    @Transactional
    fun testNonExistingUserCannotObtainJWT() {
        val params = LinkedMultiValueMap<String, String>()
        params.add("grant_type", "password")
        params.add("client_id", "devglan-client")
        params.add("username", "doiazhdoiazod@ejfoiazjfoizaj.com")
        params.add("password", "123456")

        mockMvc.perform(post("/oauth/token", params).params(params)
                .with(httpBasic("devglan-client", "devglan-secret"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError)
                .andExpect(content().contentType("application/json;charset=UTF-8"))
    }

    // works
    private fun getToken(): String {
        val params = LinkedMultiValueMap<String, String>()
        params.add("grant_type", "password")
        params.add("client_id", "devglan-client")
        params.add("username", email)
        params.add("password", password)

        val resultString = mockMvc.perform(post("/oauth/token", params).params(params)
                .with(httpBasic("devglan-client", "devglan-secret"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andReturn().response.contentAsString
        val jsonParser = JacksonJsonParser()
        return jsonParser.parseMap(resultString)["access_token"].toString()
    }

    @Test
    @Transactional
    fun testAPIEndPointsNoToken() {
        // when token required, given no token in params , then resources refused
        mockMvc.perform(get("/api/user/{email}", email)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized)
                .andExpect { status().is4xxClientError }
                .andExpect(content().contentType("application/json;charset=UTF-8"))
    }

    @Test
    @Transactional
    fun testAPIEndPointsWithToken() {
        // when token required, given token in params , then resources allowed
        val token = getToken()
        
        println("token: $token")
        val contentAsString = mockMvc.perform(get("/api/user/{email}", email)
                .header("Authorization", "Bearer $token")
                .param("email", email)
                .content("{email: $email}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()
                .response.contentAsString

        println(contentAsString)
    }
}
