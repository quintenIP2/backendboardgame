package be.kdg.boardgame.authentication.controller

import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.authentication.dto.UserDTO
import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.authentication.persistence.UserRepository
import junit.framework.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import javax.transaction.Transactional
import javax.validation.Validation
import javax.validation.Validator
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*


@SpringBootTest
@RunWith(SpringRunner::class)
@AutoConfigureMockMvc
class AuthControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private val encoder: BCryptPasswordEncoder = BCryptPasswordEncoder()
    private var validator: Validator? = null

    private lateinit var user: UserDTO
    private var url = ""


    @Before
    fun setUp() {
        url = "http://localhost:8080"
        val factory = Validation.buildDefaultValidatorFactory()
        validator = factory.validator
        user = UserDTO("username1", "username1@gmail.com", "avatar1", true)
    }

    @Test
    @Transactional
    fun correctPasswords() {
        // password pattern testing
        val passwords = listOf("ABCdef123", "ABCdef!..")
        passwords.forEach {
            val goodRegister = SignUpForm(it, "$it@gmail.com", it)
            val goodUser = User(goodRegister.email, encoder.encode(goodRegister.password), goodRegister.username)
            val violations = validator?.validate(goodUser)
            assertTrue(violations!!.isEmpty())
        }

        val mockUserData = passwords[0]
        val mockUser = User(mockUserData, encoder.encode(mockUserData), "$mockUserData@gmail.com")

        // test password encoding
        assertTrue("password is not equal to hashed password", encoder.matches("ABCdef123", mockUser.password))
        assertFalse("password is equal to hashed password", encoder.matches("jonathan", mockUser.password))
    }

    @Test
    @Transactional
    fun repositoryMock() {
        val mockUser = User("email", encoder.encode("password"), "test@gmail.com")
        val mockRepo = mock(UserRepository::class.java)
        Mockito.`when`(mockRepo.save(mockUser)).thenAnswer { mockUser }
    }

    @Test
    @Transactional
    fun testRegisterUser() {
        val body = "{\n" +
                "\t\"username\":\"jojo\",\n" +
                "\t\"email\":\"jonathanp@gmail.com\",\n" +
                "\t\"password\":\"jonathan\"\n" +
                "}\n"
        mockMvc.perform(post("${this.url}/api/register")
                .contentType(MediaType.APPLICATION_JSON).content(body))
                .andExpect(status().isOk)
    }

    @Test
    @Transactional
    fun testFailToGetUserDetails() {
        mockMvc.perform(get("${this.url}/user/{email}", user.email))
                .andExpect(status().is4xxClientError)
    }
}
