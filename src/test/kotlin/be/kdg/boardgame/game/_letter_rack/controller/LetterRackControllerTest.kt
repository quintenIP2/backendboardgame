package be.kdg.boardgame.game._letter_rack.controller

import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._game.service.GameService
import be.kdg.boardgame.game._letter_rack.dto.LetterRackDTO
import be.kdg.boardgame.game._letter_rack.dto.LetterRackDrawDTO
import be.kdg.boardgame.game._letter_rack.dto.LetterRackUpdateDTO
import be.kdg.boardgame.game._letter_rack.models.LetterRack
import be.kdg.boardgame.game._letter_rack.services.LetterRackService
import be.kdg.boardgame.game._letter_rack.services.LetterService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.oauth2.common.util.JacksonJsonParser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.LinkedMultiValueMap

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner::class)
class LetterRackControllerTest {
    @Autowired
    private lateinit var mockMvc: MockMvc
    @Autowired
    private lateinit var gameService: GameService
    @Autowired
    private lateinit var mapper: ObjectMapper
    @Autowired
    private lateinit var letterRackService: LetterRackService
    @Autowired
    private lateinit var letterService: LetterService

    private var accessToken: String? = null
    private val maxLettersInLetterRack = 7
    private val username = "test"
    private val email = "test@mail.com"
    private val password = "test"
    private val gameName = "testGame"
    private val language = "en_US"
    private var rack: LetterRack? = null

    private var game: Game? = null

    @Before
    @Transactional
    fun setUp() {
        register(username, email, password)
        accessToken = getToken(email, password)

        game = gameService.createGame(email, gameName, language, mutableListOf("None", "None", "None"))
        game = gameService.startGame(email, game!!.id)
        rack = game?.racks?.first()
    }

    @Test
    @Transactional
    fun getLetterRack() {
        val result = mockMvc.perform(get("/api/letterracks/${game!!.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isOk)
                .andReturn()

        val response = result.response.contentAsString
        checkResponseBody(response, maxLettersInLetterRack)
    }

    @Test
    @Transactional
    fun getLetterRackWrongCredentials() {
        mockMvc.perform(get("/api/letterracks/${game!!.id}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized)
    }

    @Test
    @Transactional
    fun getLetterRackWrongGame() {
        val wrongGameId = "xkdjgjhxgjhgxj"

        mockMvc.perform(get("/api/letterracks/$wrongGameId")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isNoContent)
    }

    @Test
    @Transactional
    fun removeTiles() {

        val request = mapper.writeValueAsString(LetterRackUpdateDTO(
                listOf(rack!!.tiles.random()).map {
                    it.letter.char
                }
        ))

        val result = mockMvc.perform(delete("/api/letterracks/${game!!.id}/tiles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isOk)
                .andReturn()

        val response = result.response.contentAsString
        checkResponseBody(response, maxLettersInLetterRack - 1)
    }

    @Test
    @Transactional
    fun removeTilesNotInLetterRack() {
        // get all possible letters
        val letters = letterService.getAllLettersByLanguage(language).toMutableList()
        // remove all letters in the player's letterrack
        letters.removeIf { letter ->
            rack!!.tiles.any { tile ->
                tile.letter.char == letter.char
            }
        }

        // pick a random letter out of the remaining ones
        val request = mapper.writeValueAsString(LetterRackUpdateDTO(
                listOf(letters.random()).map {
                    it.char
                }
        ))
        val result = mockMvc.perform(delete("/api/letterracks/${game!!.id}/tiles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isOk)
                .andReturn()

        val response = result.response.contentAsString
        checkResponseBody(response, maxLettersInLetterRack)
    }

    @Test
    @Transactional
    fun removeTilesWrongCredentials() {
        val request = mapper.writeValueAsString(LetterRackUpdateDTO(
                listOf(rack!!.tiles.random()).map {
                    it.letter.char
                }
        ))
        mockMvc.perform(delete("/api/letterracks/${game!!.id}/tiles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                .andExpect(status().isUnauthorized)
    }

    @Test
    @Transactional
    fun removeTilesWrongGame() {
        val request = mapper.writeValueAsString(LetterRackUpdateDTO(
                listOf(rack!!.tiles.random()).map {
                    it.letter.char
                }
        ))
        val wrongGameId = "jhxgfjxjhvb"

        mockMvc.perform(delete("/api/letterracks/$wrongGameId/tiles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isNoContent)
    }

    @Test
    @Transactional
    fun replaceTiles() {
        val request = mapper.writeValueAsString(LetterRackUpdateDTO(
                listOf(rack!!.tiles.random()).map {
                    it.letter.char
                }
        ))

        val result = mockMvc.perform(put("/api/letterracks/${game!!.id}/tiles/replace")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isOk)
                .andReturn()

        val response = result.response.contentAsString
        checkResponseBody(response, maxLettersInLetterRack)
    }

    @Test
    @Transactional
    fun replaceTilesNotInLetterRack() {
        // get all possible letters
        val letters = letterService.getAllLettersByLanguage(language).toMutableList()
        // remove all letters in the player's letterrack
        letters.removeIf { letter ->
            rack!!.tiles.any { tile ->
                tile.letter.char == letter.char
            }
        }

        // pick a random letter out of the remaining ones
        val request = mapper.writeValueAsString(LetterRackUpdateDTO(
                listOf(letters.random()).map {
                    it.char
                }
        ))
        val result = mockMvc.perform(put("/api/letterracks/${game!!.id}/tiles/replace")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isOk)
                .andReturn()

        val response = result.response.contentAsString
        checkResponseBody(response, maxLettersInLetterRack)
    }

    @Test
    @Transactional
    fun replaceTilesWrongCredentials() {
        val request = mapper.writeValueAsString(LetterRackUpdateDTO(
                listOf(rack!!.tiles.random()).map {
                    it.letter.char
                }
        ))
        mockMvc.perform(put("/api/letterracks/${game!!.id}/tiles/replace")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                .andExpect(status().isUnauthorized)
    }

    @Test
    @Transactional
    fun replaceTilesWrongGame() {
        val request = mapper.writeValueAsString(LetterRackUpdateDTO(
                listOf(rack!!.tiles.random()).map {
                    it.letter.char
                }
        ))
        val wrongGameId = "jhxgfjxjhvb"

        mockMvc.perform(put("/api/letterracks/$wrongGameId/tiles/replace")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isNoContent)
    }

    @Test
    @Transactional
    fun addRandomTiles() {
        // remove 4 tiles
        letterRackService.removeTiles(
                email, game!!.id,
                rack!!.tiles.slice(0 until 4).map {
                    it.letter.char
                }.toTypedArray()
        )

        val request = mapper.writeValueAsString(LetterRackDrawDTO(3))

        val result = mockMvc.perform(post("/api/letterracks/${game!!.id}/tiles/random")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isOk)
                .andReturn()

        val response = result.response.contentAsString
        checkResponseBody(response, maxLettersInLetterRack - 1)
    }

    @Test
    @Transactional
    fun addTooMuchRandomTiles() {
        // remove 4 tiles
        letterRackService.removeTiles(
                email, game!!.id,
                rack!!.tiles.slice(0 until 3).map {
                    it.letter.char
                }.toTypedArray()
        )

        val request = mapper.writeValueAsString(LetterRackDrawDTO(8))

        val result = mockMvc.perform(post("/api/letterracks/${game!!.id}/tiles/random")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isOk)
                .andReturn()

        val response = result.response.contentAsString
        checkResponseBody(response, maxLettersInLetterRack)
    }

    @Test
    @Transactional
    fun addRandomTilesWrongCredentials() {
        val request = mapper.writeValueAsString(LetterRackDrawDTO(3))

        mockMvc.perform(post("/api/letterracks/${game!!.id}/tiles/random")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                .andExpect(status().isUnauthorized)
    }

    @Test
    @Transactional
    fun addRandomTilesWrongGame() {
        val request = mapper.writeValueAsString(LetterRackDrawDTO(3))
        val wrongGameId = "xkhdgvjhxhjkvhbj"

        mockMvc.perform(post("/api/letterracks/$wrongGameId/tiles/random")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isNoContent)
    }

    private fun checkResponseBody(body: String?, lettersInLetterRack: Int) {
        // check if response body is not empty
        assert(body?.isNotBlank() ?: false)

        // check if response is sensible
        val letterRack: LetterRackDTO = mapper.readValue(body, LetterRackDTO::class.java)!!
        letterRack.game!!
        assert(letterRack.tiles.size == lettersInLetterRack)
    }

    // registration
    private fun register(username: String, email: String, password: String) {
        val body = mapper.writeValueAsString(SignUpForm(username, email, password))

        mockMvc.perform(MockMvcRequestBuilders.post("/api/register")
                .contentType(MediaType.APPLICATION_JSON).content(body))
    }

    private fun getToken(email: String, password: String): String {
        val params = LinkedMultiValueMap<String, String>()
        params.add("grant_type", "password")
        params.add("client_id", "devglan-client")
        params.add("username", email)
        params.add("password", password)

        val resultString = mockMvc.perform(MockMvcRequestBuilders.post("/oauth/token", params).params(params)
                .with(SecurityMockMvcRequestPostProcessors.httpBasic("devglan-client", "devglan-secret"))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().response.contentAsString
        val jsonParser = JacksonJsonParser()
        return jsonParser.parseMap(resultString)["access_token"].toString()
    }
}
