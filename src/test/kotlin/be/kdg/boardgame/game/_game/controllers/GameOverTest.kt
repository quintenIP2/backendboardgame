package be.kdg.boardgame.game._game.controllers

import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._game.dto.GameDTO
import be.kdg.boardgame.game._game.dto.LaidDownTileDTO
import be.kdg.boardgame.game._game.dto.PlaceTilesRequestDTO
import be.kdg.boardgame.game._game.dto.PlayedTurnDTO
import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._game.persistence.GameRepository
import be.kdg.boardgame.game._game.service.GameService
import be.kdg.boardgame.game._letter_rack.dto.LetterRackDTO
import be.kdg.boardgame.game._letter_rack.dto.LetterRackDrawDTO
import be.kdg.boardgame.game._letter_rack.persistence.LetterRackRepository
import be.kdg.boardgame.game._letter_rack.services.LetterRackService
import be.kdg.boardgame.game._letter_rack.services.LetterService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.oauth2.common.util.JacksonJsonParser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.util.LinkedMultiValueMap
import javax.transaction.Transactional

@SpringBootTest
@RunWith(SpringRunner::class)
@AutoConfigureMockMvc
class GameOverTest {
    @Autowired
    private lateinit var mockMvc: MockMvc
    @Autowired
    private lateinit var mapper: ObjectMapper
    @Autowired
    private lateinit var gameService: GameService
    @Autowired
    private lateinit var letterRackService: LetterRackService
    @Autowired
    private lateinit var letterService: LetterService

    @Autowired
    private lateinit var letterRackRepository: LetterRackRepository
    @Autowired
    private lateinit var gameRepository: GameRepository

    private val email1 = "test1@mail.com"
    private val username1 = "TestyMcTestFace_1"
    private val password1 = "test1"
    private var token1 = ""

    private val email2 = "test2@mail.com"
    private val username2 = "TestyMcTestFace_2"
    private val password2 = "test2"
    private var token2 = ""

    private val gameTitle = "testGame"
    private val gameLanguage = "en_US"

    private var game: Game? = null

    @Before
    @Transactional
    fun setup() {
        register(username1, email1, password1)
        token1 = getToken(email1, password1)

        register(username2, email2, password2)
        token2 = getToken(email2, password2)

        game = gameService.createGame(email1, gameTitle, gameLanguage, mutableListOf("TestyMcTestFace_2", "None", "None"))
        game = gameService.startGame(email1, game!!.id)

        setupBoard()
        setupLetterRacks()
    }

    @Test
    @Transactional
    fun playTwoSkipsPerPlayer() {
        val skipRequest = PlaceTilesRequestDTO(emptyList(), game!!.id)

        // Round 1
        placeTiles(skipRequest, token1)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        placeTiles(skipRequest, token2)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))

        // Round 2
        placeTiles(skipRequest, token1)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        val response = placeTiles(skipRequest, token2)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        // Assert response
        val turn = mapper.readValue(response, PlayedTurnDTO::class.java)

        assert(turn.over)
        assertScores(listOf(-9, -16), token1)
    }

    @Test
    @Transactional
    fun emptyLetterBag() {
        // Empty letter bag
        game!!.letterBag.removeAll { true }

        // Place tiles
        val request = PlaceTilesRequestDTO(listOf(
                LaidDownTileDTO('S', 9, 13),
                LaidDownTileDTO('B', 11, 13),
                LaidDownTileDTO('D', 12, 13),
                LaidDownTileDTO('.', 13, 13),
                LaidDownTileDTO('E', 14, 13)
        ))
        placeTiles(request, token1)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        val response = drawTiles(LetterRackDrawDTO(request.tiles.size), token1)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        val letterRack = mapper.readValue(response, LetterRackDTO::class.java)

        val lettersInRack2 = letterRackService.getLetterRackForPlayerInGame(email2, game!!.id).tiles.size
        assert(letterRack.tiles.size < lettersInRack2)
    }

    @Test
    @Transactional
    fun emptyLetterBagPlayAllTiles() {
        // Empty letter bag
        game!!.letterBag.removeAll { true }

        // Place tiles
        // ABASED.
        var request = PlaceTilesRequestDTO(listOf(
                LaidDownTileDTO('S', 9, 13),
                LaidDownTileDTO('B', 11, 13),
                LaidDownTileDTO('D', 12, 13),
                LaidDownTileDTO('.', 13, 13),
                LaidDownTileDTO('E', 14, 13)
        ))
        placeTiles(request, token1)

        // ZOOLOG.
        request = PlaceTilesRequestDTO(listOf(
                LaidDownTileDTO('L', 13, 12),
                LaidDownTileDTO('G', 13, 14)
        ))
        placeTiles(request, token2)

        // AA
        request = PlaceTilesRequestDTO(listOf(
                LaidDownTileDTO('A', 4, 10)
        ))
        placeTiles(request, token1)

        // ZOOO.
        request = PlaceTilesRequestDTO(listOf(
                LaidDownTileDTO('O', 14, 0)
        ))
        placeTiles(request, token2)

        // A
        request = PlaceTilesRequestDTO(listOf(
                LaidDownTileDTO('A', 1, 12)
        ))

        val response = placeTiles(request, token1)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        val turn = mapper.readValue(response, PlayedTurnDTO::class.java)
        assert(turn.over)
        assertScores(listOf(36, -7), token1)
    }

    @Test
    @Transactional
    fun emptyLetterBagSkipTurns() {
        // Empty letter bag
        game!!.letterBag.removeAll { true }

        val skipRequest = PlaceTilesRequestDTO(emptyList(), game!!.id)

        // Round 1
        placeTiles(skipRequest, token1)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        placeTiles(skipRequest, token2)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))

        // Round 2
        placeTiles(skipRequest, token1)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        val response = placeTiles(skipRequest, token2)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        val turn = mapper.readValue(response, PlayedTurnDTO::class.java)
        assert(turn.over)
        assertScores(listOf(-9, -16), token1)
    }

    private fun assertScores(expected: List<Int>, token: String) {
        val response = mockMvc.perform(
                get("/api/games/${game!!.id}")
                        .header("Authorization", "Bearer $token"))
                .andExpect(status().isOk)
                .andReturn().response.contentAsString

        val game = mapper.readValue(response, GameDTO::class.java)
        val actual = game.scores.map { it.score }

        assert(actual.zip(expected).all { it.first == it.second })
    }

    private fun placeTiles(requestBody: PlaceTilesRequestDTO, token: String) = mockMvc.perform(
            post("/api/games/${game!!.id}/tiles")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .accept(MediaType.APPLICATION_JSON_UTF8)
                    .header("Authorization", "Bearer $token")
                    .content(mapper.writeValueAsString(requestBody))
    )

    private fun drawTiles(requestBody: LetterRackDrawDTO, token: String) = mockMvc.perform(
            post("/api/letterracks/${game!!.id}/tiles/random")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .accept(MediaType.APPLICATION_JSON_UTF8)
                    .header("Authorization", "Bearer $token")
                    .content(mapper.writeValueAsString(requestBody))
    )

    // Game setup
    private fun setupLetterRacks() {
        val rack1 = letterRackService.getLetterRackForPlayerInGame(email1, game!!.id)
        rack1.tiles.removeAll { true }
        rack1.tiles.addAll(listOf(
                Tile(letterService.getLetterByCharAndLanguage('A', gameLanguage), rack1),
                Tile(letterService.getLetterByCharAndLanguage('B', gameLanguage), rack1),
                Tile(letterService.getLetterByCharAndLanguage('A', gameLanguage), rack1),
                Tile(letterService.getLetterByCharAndLanguage('S', gameLanguage), rack1),
                Tile(letterService.getLetterByCharAndLanguage('E', gameLanguage), rack1),
                Tile(letterService.getLetterByCharAndLanguage('D', gameLanguage), rack1),
                Tile(letterService.getLetterByCharAndLanguage('.', gameLanguage), rack1)
        ))

        letterRackRepository.save(rack1)

        val rack2 = letterRackService.getLetterRackForPlayerInGame(email2, game!!.id)
        rack2.tiles.removeAll { true }
        rack2.tiles.addAll(listOf(
                Tile(letterService.getLetterByCharAndLanguage('Z', gameLanguage), rack2),
                Tile(letterService.getLetterByCharAndLanguage('O', gameLanguage), rack2),
                Tile(letterService.getLetterByCharAndLanguage('O', gameLanguage), rack2),
                Tile(letterService.getLetterByCharAndLanguage('L', gameLanguage), rack2),
                Tile(letterService.getLetterByCharAndLanguage('O', gameLanguage), rack2),
                Tile(letterService.getLetterByCharAndLanguage('G', gameLanguage), rack2),
                Tile(letterService.getLetterByCharAndLanguage('.', gameLanguage), rack2)
        ))

        letterRackRepository.save(rack2)
    }

    private fun setupBoard() {
        val board = arrayOf(
                "_______________",
                "_________TOWNIE",
                "_______R___R_N_",
                "_____F_U_D_I_T_",
                "___GALABIA_T_E_",
                "___L_E___M_E_R_",
                "___O_S__M____V_",
                "_FEW_HIJACK_HAO",
                "_E______N___EL_",
                "IDES____G_Y_L_R",
                "_S_T___VORACITY",
                "___E______N_X_E",
                "___E_P____Q____",
                "ERUPTION__U____",
                "_____N_U__I____"
        )
        var x = 0
        var y = 0
        board.forEach rows@ { word ->
            word.forEach cols@ {
                if (it != '_') {
                    val i = y * game!!.board!!.width + x
                    game!!.board!!.cells[i].tile = Tile(letterService.getLetterByCharAndLanguage(it, gameLanguage), cell = game!!.board!!.cells[i])
                    game!!.board!!.cells[i].used = true
                }
                x++
            }
            y++
            x = 0
        }

        gameRepository.save(game as Game)
    }

    // Authentication
    private fun register(username: String, email: String, password: String) {
        val body = mapper.writeValueAsString(SignUpForm(username, email, password))

        mockMvc.perform(MockMvcRequestBuilders.post("/api/register")
                .contentType(MediaType.APPLICATION_JSON).content(body))
    }

    private fun getToken(email: String, password: String): String {
        val params = LinkedMultiValueMap<String, String>()
        params.add("grant_type", "password")
        params.add("client_id", "devglan-client")
        params.add("username", email)
        params.add("password", password)

        val resultString = mockMvc.perform(MockMvcRequestBuilders.post("/oauth/token", params).params(params)
                .with(SecurityMockMvcRequestPostProcessors.httpBasic("devglan-client", "devglan-secret"))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().response.contentAsString
        val jsonParser = JacksonJsonParser()
        return jsonParser.parseMap(resultString)["access_token"].toString()
    }
}
