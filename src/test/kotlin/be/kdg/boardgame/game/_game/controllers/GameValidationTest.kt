package be.kdg.boardgame.game._game.controllers

import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._game.dto.GameDTO
import be.kdg.boardgame.game._game.dto.LaidDownTileDTO
import be.kdg.boardgame.game._game.dto.PlaceTilesRequestDTO
import be.kdg.boardgame.game._game.dto.PlayedTurnDTO
import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._game.persistence.GameRepository
import be.kdg.boardgame.game._game.service.GameService
import be.kdg.boardgame.game._letter_rack.persistence.LetterRackRepository
import be.kdg.boardgame.game._letter_rack.services.LetterRackService
import be.kdg.boardgame.game._letter_rack.services.LetterService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.oauth2.common.util.JacksonJsonParser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.LinkedMultiValueMap

@SpringBootTest
@RunWith(SpringRunner::class)
@AutoConfigureMockMvc
class GameValidationTest {
    @Autowired
    private lateinit var mockMvc: MockMvc
    @Autowired
    private lateinit var mapper: ObjectMapper
    @Autowired
    private lateinit var gameService: GameService
    @Autowired
    private lateinit var letterRackService: LetterRackService
    @Autowired
    private lateinit var letterService: LetterService
    @Autowired
    private lateinit var letterRackRepository: LetterRackRepository
    @Autowired
    private lateinit var gameRepository: GameRepository

    private val email = "test@mail.com"
    private val username = "TestyMcTestFace"
    private val password = "test"
    private var token = ""

    private val wrongGameTitle = "wrongGame"
    private val gameTitle = "testGame"
    private val gameLanguage = "en_US"
    private var game: Game? = null

    @Before
    @Transactional
    fun setup() {
        register(username, email, password)
        token = getToken(email, password)

        game = gameService.createGame(email, gameTitle, gameLanguage, mutableListOf("None", "None", "None"))
        game = gameService.startGame(email, game!!.id)

        setupLetterRack()
    }

    @After
    @Transactional
    fun breakdown() {
        gameService.deleteGame(game!!.id, email)
    }

    @Test
    @Transactional
    fun playWordHorizontal() {
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 5, 7),
                LaidDownTileDTO('B', 6, 7),
                LaidDownTileDTO('A', 7, 7),
                LaidDownTileDTO('S', 8, 7),
                LaidDownTileDTO('E', 9, 7)
        )

        val result = layTiles(tilesToPlay)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        checkResponseBody(result, arrayOf("ABASE"), arrayOf(14), "ABASE 8F +14")

        assertNotEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playWordVertical() {
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 7, 5),
                LaidDownTileDTO('B', 7, 6),
                LaidDownTileDTO('A', 7, 7),
                LaidDownTileDTO('S', 7, 8),
                LaidDownTileDTO('E', 7, 9)
        )

        val result = layTiles(tilesToPlay)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        checkResponseBody(result, arrayOf("ABASE"), arrayOf(14), "ABASE H6 +14")

        assertNotEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playWordWithPlaceholder() {
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 5, 7),
                LaidDownTileDTO('B', 6, 7),
                LaidDownTileDTO('A', 7, 7),
                LaidDownTileDTO('.', 8, 7),
                LaidDownTileDTO('E', 9, 7)
        )

        val result = layTiles(tilesToPlay)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        checkResponseBody(result, arrayOf("ABASE"), arrayOf(12), "ABAsE 8F +12")

        assertNotEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playFirstWordNotInCenter() {
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 5, 8),
                LaidDownTileDTO('B', 6, 8),
                LaidDownTileDTO('A', 7, 8),
                LaidDownTileDTO('S', 8, 8),
                LaidDownTileDTO('E', 9, 8)
        )

        layTiles(tilesToPlay)
                .andExpect(status().isForbidden)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))

        assertEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playWordWithLetterNotInRack() {
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 5, 7),
                LaidDownTileDTO('B', 6, 7),
                LaidDownTileDTO('A', 7, 7),
                LaidDownTileDTO('S', 8, 7),
                LaidDownTileDTO('E', 9, 7),
                LaidDownTileDTO('R', 10, 7)
        )

        layTiles(tilesToPlay)
                .andExpect(status().isForbidden)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))

        assertEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playInvalidWord() {
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('S', 5, 7),
                LaidDownTileDTO('B', 6, 7),
                LaidDownTileDTO('A', 7, 7),
                LaidDownTileDTO('A', 8, 7),
                LaidDownTileDTO('E', 9, 7)
        )

        layTiles(tilesToPlay)
                .andExpect(status().isForbidden)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))

        assertEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playWordNotInLine() {
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 5, 5),
                LaidDownTileDTO('B', 6, 6),
                LaidDownTileDTO('A', 7, 7),
                LaidDownTileDTO('S', 8, 8),
                LaidDownTileDTO('E', 9, 9)
        )

        layTiles(tilesToPlay)
                .andExpect(status().isForbidden)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))

        assertEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playWordDoubleWord() {
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 5, 7),
                LaidDownTileDTO('B', 6, 7),
                LaidDownTileDTO('A', 7, 7),
                LaidDownTileDTO('S', 8, 7),
                LaidDownTileDTO('E', 9, 7)
        )

        val result = layTiles(tilesToPlay)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        checkResponseBody(result, arrayOf("ABASE"), arrayOf(14), "ABASE 8F +14")

        assertNotEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playWordTripleWord() {
        setupBoard()
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 0, 0),
                LaidDownTileDTO('B', 0, 1),
                LaidDownTileDTO('A', 0, 2),
                LaidDownTileDTO('S', 0, 3),
                LaidDownTileDTO('E', 0, 4),
                LaidDownTileDTO('D', 0, 5),
                LaidDownTileDTO('.', 0, 6)
        )

        val result = layTiles(tilesToPlay)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        checkResponseBody(result, arrayOf("ABASEDLY"), arrayOf(92), "ABASEDl(Y) A1 +92")

        assertNotEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playWordDoubleLetter() {
        setupBoard()
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('B', 2, 5),
                LaidDownTileDTO('A', 2, 6)
        )

        val result = layTiles(tilesToPlay)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        checkResponseBody(result, arrayOf("BAM"), arrayOf(8), "BA(M) C6 +8")

        assertNotEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playWordTripleLetter() {
        setupBoard()
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 1, 5),
                LaidDownTileDTO('B', 1, 6),
                LaidDownTileDTO('S', 1, 8),
                LaidDownTileDTO('E', 1, 9)
        )

        val result = layTiles(tilesToPlay)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        checkResponseBody(result, arrayOf("ABUSE"), arrayOf(11), "AB(U)SE B6 +11")

        assertNotEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playBingo() {
        setupBoard()
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 0, 0),
                LaidDownTileDTO('B', 0, 1),
                LaidDownTileDTO('A', 0, 2),
                LaidDownTileDTO('S', 0, 3),
                LaidDownTileDTO('E', 0, 4),
                LaidDownTileDTO('D', 0, 5),
                LaidDownTileDTO('.', 0, 6)
        )

        val result = layTiles(tilesToPlay)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        checkResponseBody(result, arrayOf("ABASEDLY"), arrayOf(92), "ABASEDl(Y) A1 +92")

        assertNotEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playWordNotConnectedToWordsOnBoard() {
        setupBoard()
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 0, 0),
                LaidDownTileDTO('B', 1, 0),
                LaidDownTileDTO('A', 2, 0),
                LaidDownTileDTO('S', 3, 0),
                LaidDownTileDTO('E', 4, 0),
                LaidDownTileDTO('D', 5, 0)
        )

        layTiles(tilesToPlay)
                .andExpect(status().isForbidden)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))

        assertEquals(boardBefore, getBoard())
    }

    @Test
    @Transactional
    fun playWordWrongCredentials() {
        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 6, 8),
                LaidDownTileDTO('B', 7, 8),
                LaidDownTileDTO('A', 8, 8),
                LaidDownTileDTO('S', 9, 8),
                LaidDownTileDTO('E', 10, 8)
        )

        val request = mapper.writeValueAsString(PlaceTilesRequestDTO(tilesToPlay))

        mockMvc.perform(post("/api/games/${game!!.id}/tiles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                .andExpect(status().isUnauthorized)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
    }

    @Test
    @Transactional
    fun playWordWrongGame() {
        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 6, 8),
                LaidDownTileDTO('B', 7, 8),
                LaidDownTileDTO('A', 8, 8),
                LaidDownTileDTO('S', 9, 8),
                LaidDownTileDTO('E', 10, 8)
        )

        val request = mapper.writeValueAsString(PlaceTilesRequestDTO(tilesToPlay))

        mockMvc.perform(post("/api/games/$wrongGameTitle/tiles")
                .header("Authorization", "Bearer $token")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                .andExpect(status().isNoContent)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))

    }

    @Test
    @Transactional
    fun playOneTile() {
        setupBoard()
        val boardBefore = getBoard()

        val tilesToPlay = listOf(
                LaidDownTileDTO('.', 4, 6)
        )

        val result = layTiles(tilesToPlay)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response.contentAsString

        checkResponseBody(result, arrayOf("AI"), arrayOf(1), "a(I) E7 +1")

        assertNotEquals(boardBefore, getBoard())
    }

    private fun setupLetterRack() {
        val rack = letterRackService.getLetterRackForPlayerInGame(email, game!!.id)
        rack.tiles.removeAll { true }
        rack.tiles.addAll(listOf(
                Tile(letterService.getLetterByCharAndLanguage('A', gameLanguage), rack),
                Tile(letterService.getLetterByCharAndLanguage('B', gameLanguage), rack),
                Tile(letterService.getLetterByCharAndLanguage('A', gameLanguage), rack),
                Tile(letterService.getLetterByCharAndLanguage('S', gameLanguage), rack),
                Tile(letterService.getLetterByCharAndLanguage('E', gameLanguage), rack),
                Tile(letterService.getLetterByCharAndLanguage('D', gameLanguage), rack),
                Tile(letterService.getLetterByCharAndLanguage('.', gameLanguage), rack)
        ))

        letterRackRepository.save(rack)
    }

    private fun setupBoard() {
        val word = "YUMMIEST"
        var i = 0

        word.forEach {
            game!!.board!!.cells[105 + i].tile = Tile(letterService.getLetterByCharAndLanguage(it, gameLanguage), cell = game!!.board!!.cells[105 + i])
            game!!.board!!.cells[105 + i].used = true
            i++
        }

        gameRepository.save(game as Game)
    }

    private fun layTiles(tiles: List<LaidDownTileDTO>): ResultActions {
        val request = mapper.writeValueAsString(PlaceTilesRequestDTO(tiles))

        return mockMvc.perform(
                post("/api/games/${game!!.id}/tiles")
                        .header("Authorization", "Bearer $token")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request)
        )
    }

    private fun checkResponseBody(body: String?, words: Array<String>, scores: Array<Int>, expectedRepresentation: String) {
        // check if response body is not empty
        assert(body?.isNotBlank() ?: false)

        // check if response is sensible
        val playedTurn = mapper.readValue(body, PlayedTurnDTO::class.java)!!

        for (i in 0 until words.size) {
            assertEquals(words[i], playedTurn.words[i].word.replace("(", "").replace(")", "").toUpperCase())
            assertEquals(scores[i], playedTurn.words[i].score)
        }

        assertEquals(expectedRepresentation, playedTurn.representation)
    }

    private fun getBoard(): String {
        val response = mockMvc.perform(
                get("/api/games/${game!!.id}")
                        .header("Authorization", "Bearer $token")
                        .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk)
                .andReturn()
                .response.contentAsString
        val g = mapper.readValue(response, GameDTO::class.java)
        return g.board
    }

    private fun register(username: String, email: String, password: String) {
        val body = mapper.writeValueAsString(SignUpForm(username, email, password))

        mockMvc.perform(MockMvcRequestBuilders.post("/api/register")
                .contentType(MediaType.APPLICATION_JSON).content(body))
    }

    private fun getToken(email: String, password: String): String {
        val params = LinkedMultiValueMap<String, String>()
        params.add("grant_type", "password")
        params.add("client_id", "devglan-client")
        params.add("username", email)
        params.add("password", password)

        val resultString = mockMvc.perform(MockMvcRequestBuilders.post("/oauth/token", params).params(params)
                .with(SecurityMockMvcRequestPostProcessors.httpBasic("devglan-client", "devglan-secret"))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().response.contentAsString
        val jsonParser = JacksonJsonParser()
        return jsonParser.parseMap(resultString)["access_token"].toString()
    }
}
