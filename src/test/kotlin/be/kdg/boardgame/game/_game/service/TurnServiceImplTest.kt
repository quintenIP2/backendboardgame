package be.kdg.boardgame.game._game.service

import be.kdg.boardgame.authentication.models.User
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._game.dto.PlaceTilesRequestDTO
import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._letter_rack.models.Letter
import be.kdg.boardgame.game._letter_rack.models.LetterRack
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class TurnServiceImplTest {
    @Autowired
    private lateinit var turnService: TurnService
    lateinit var turns: MutableList<PlaceTilesRequestDTO>
    lateinit var letterRacks: MutableList<LetterRack>

    @Before
    fun setUp() {
        letterRacks = mutableListOf(
                LetterRack(User(), Game(), mutableListOf(
                        Tile(Letter('a', "en", 1)),
                        Tile(Letter('b', "en", 1)),
                        Tile(Letter('c', "en", 1)))
                ),
                LetterRack(User(), Game(), mutableListOf(
                        Tile(Letter('a', "en", 1)),
                        Tile(Letter('b', "en", 1)),
                        Tile(Letter('c', "en", 1)))
                ),
                LetterRack(User(), Game(), mutableListOf(
                        Tile(Letter('a', "en", 1)),
                        Tile(Letter('b', "en", 1)),
                        Tile(Letter('c', "en", 1)))
                )
        )
    }

    @Test
    fun stringifyRacks() {
        val result = turnService.stringifyPlayerRacks(letterRacks)
        val splittedRacks = result.trimEnd('#').split('#')
        assertTrue(splittedRacks.size == 3)
    }
}
