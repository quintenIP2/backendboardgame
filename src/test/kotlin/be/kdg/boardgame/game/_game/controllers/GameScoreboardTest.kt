package be.kdg.boardgame.game._game.controllers

import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._game.dto.*
import be.kdg.boardgame.game._letter_rack.persistence.LetterRackRepository
import be.kdg.boardgame.game._letter_rack.services.LetterRackService
import be.kdg.boardgame.game._letter_rack.services.LetterService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.oauth2.common.util.JacksonJsonParser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.util.LinkedMultiValueMap
import javax.transaction.Transactional

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["spring.profiles.active=test"])
@AutoConfigureMockMvc
class GameScoreboardTest {
    @Autowired
    private lateinit var mockMvc: MockMvc
    @Autowired
    private lateinit var mapper: ObjectMapper
    @Autowired
    private lateinit var letterRackService: LetterRackService
    @Autowired
    private lateinit var letterRackRepository: LetterRackRepository
    @Autowired
    private lateinit var letterService: LetterService

    private val email = "test@mail.com"
    private val username = "TestyMcTestFace"
    private val password = "test"
    private var token = ""

    private val gameTitle = "Score Test"
    private val language = "en_US"

    @Before
    @Transactional
    fun setup() {
        register(username, email, password)
        token = getToken(email, password)
    }

    @Test
    @Transactional
    fun scoresForNewGame() {
        var gameDTO = createGame(NewGameRequestDTO(gameTitle, language, mutableListOf("None", "None", "None")))
        gameDTO = startGame(gameDTO.id!!)

        gameDTO.scores.forEach {
            assert(it.score == 0)
        }
    }

    @Test
    @Transactional
    fun scoresAfterPlayingTiles() {
        // Create game
        var gameDTO = createGame(NewGameRequestDTO(gameTitle, language, mutableListOf("None", "None", "None")))
        gameDTO = startGame(gameDTO.id!!)

        setupLetterRack(gameDTO.id!!)

        val scoreBeforePlaying = gameDTO.scores[0].score

        // Play Tiles
        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 5, 7),
                LaidDownTileDTO('B', 6, 7),
                LaidDownTileDTO('A', 7, 7),
                LaidDownTileDTO('S', 8, 7),
                LaidDownTileDTO('E', 9, 7)
        )
        val turnDTO = layTiles(gameDTO.id!!, tilesToPlay)
        val scoreAfterPlaying = turnDTO.words[0].score

        // Check score
        assert(scoreBeforePlaying < scoreAfterPlaying)
    }

    @Test
    @Transactional
    fun scoresAfterPlayingInvalidTiles() {
        // Create game
        var gameDTO = createGame(NewGameRequestDTO(gameTitle, language, mutableListOf("None", "None", "None")))
        gameDTO = startGame(gameDTO.id!!)
        setupLetterRack(gameDTO.id!!)

        val scoreBeforePlaying = gameDTO.scores[0]

        // Play Tiles
        val tilesToPlay = listOf(
                LaidDownTileDTO('X', 5, 7)
        )
        val gameDTOAfter = layInvalidTiles(gameDTO.id!!, tilesToPlay)
        val scoreAfterPlaying = gameDTOAfter.scores[0]

        // Check score
        assert(scoreBeforePlaying == scoreAfterPlaying)
    }

    private fun createGame(requestDTO: NewGameRequestDTO): GameDTO {
        val newGameRequest = mapper.writeValueAsString(requestDTO)
        val response = mockMvc
                .perform(MockMvcRequestBuilders.post("/api/games/")
                        .content(newGameRequest)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .header("Authorization", "Bearer $token"))
                .andReturn().response.contentAsString
        return mapper.readValue(response, GameDTO::class.java)
    }

    private fun startGame(gameId: String): GameDTO {
        val response = mockMvc
                .perform(MockMvcRequestBuilders.post("/api/games/$gameId")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .header("Authorization", "Bearer $token"))
                .andReturn().response.contentAsString
        return mapper.readValue(response, GameDTO::class.java)
    }

    private fun layTiles(gameId: String, tiles: List<LaidDownTileDTO>): PlayedTurnDTO {
        val request = mapper.writeValueAsString(PlaceTilesRequestDTO(tiles))

        val response = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/games/$gameId/tiles")
                        .header("Authorization", "Bearer $token")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk)
                .andReturn().response.contentAsString
        return mapper.readValue(response, PlayedTurnDTO::class.java)
    }

    private fun layInvalidTiles(gameId: String, tiles: List<LaidDownTileDTO>): GameDTO {
        val request = mapper.writeValueAsString(PlaceTilesRequestDTO(tiles))

        mockMvc.perform(
                post("/api/games/$gameId/tiles")
                        .header("Authorization", "Bearer $token")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isForbidden)

        val response = mockMvc.perform(
                get("/api/games/$gameId")
                        .header("Authorization", "Bearer $token")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk)
                .andReturn().response.contentAsString

        return mapper.readValue(response, GameDTO::class.java)
    }

    // setup letterrack
    private fun setupLetterRack(gameId: String) {
        val rack = letterRackService.getLetterRackForPlayerInGame(email, gameId)
        rack.tiles.removeAll { true }
        rack.tiles.addAll(listOf(
                Tile(letterService.getLetterByCharAndLanguage('A', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('B', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('A', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('S', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('E', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('D', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('.', language), rack)
        ))

        letterRackRepository.save(rack)
    }

    // Register / Bearer Token
    private fun register(username: String, email: String, password: String) {
        val body = mapper.writeValueAsString(SignUpForm(username, email, password))

        mockMvc.perform(MockMvcRequestBuilders.post("/api/register")
                .contentType(MediaType.APPLICATION_JSON).content(body))
    }

    private fun getToken(email: String, password: String): String {
        val params = LinkedMultiValueMap<String, String>()
        params.add("grant_type", "password")
        params.add("client_id", "devglan-client")
        params.add("username", email)
        params.add("password", password)

        val resultString = mockMvc.perform(MockMvcRequestBuilders.post("/oauth/token", params).params(params)
                .with(SecurityMockMvcRequestPostProcessors.httpBasic("devglan-client", "devglan-secret"))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().response.contentAsString
        val jsonParser = JacksonJsonParser()
        return jsonParser.parseMap(resultString)["access_token"].toString()
    }
}
