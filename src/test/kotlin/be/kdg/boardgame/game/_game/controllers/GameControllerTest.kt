package be.kdg.boardgame.game._game.controllers

import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.game._board.models.Tile
import be.kdg.boardgame.game._game.dto.LaidDownTileDTO
import be.kdg.boardgame.game._game.dto.NewGameRequestDTO
import be.kdg.boardgame.game._game.dto.PlaceTilesRequestDTO
import be.kdg.boardgame.game._game.model.Game
import be.kdg.boardgame.game._game.service.GameService
import be.kdg.boardgame.game._letter_rack.persistence.LetterRackRepository
import be.kdg.boardgame.game._letter_rack.services.LetterRackService
import be.kdg.boardgame.game._letter_rack.services.LetterService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.oauth2.common.util.JacksonJsonParser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.LinkedMultiValueMap

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["spring.profiles.active=test"])
@AutoConfigureMockMvc
class GameControllerTest {
    @Autowired
    private lateinit var mapper: ObjectMapper
    @Autowired
    private lateinit var mockMvc: MockMvc
    @Autowired
    private lateinit var letterRackService: LetterRackService
    @Autowired
    private lateinit var gameService: GameService
    @Autowired
    private lateinit var letterService: LetterService
    @Autowired
    private lateinit var letterRackRepository: LetterRackRepository

    private val email: String = "hasfriends@gmail.com"
    private val password: String = "hasfriends@gmail.com"
    private val username = "test"
    private var accessToken: String? = null
    private val gameName = "testGame"
    private val language = "en_US"
    private var game: Game? = null
    lateinit var turn: PlaceTilesRequestDTO
    private var jsonParser = JacksonJsonParser()
    val players = mutableListOf("None", "None", "None")

    @Before
    fun setup() {
        register(username, email, password)
        accessToken = getToken(email, password)

        game = gameService.createGame(email, gameName, language, players)
        game = gameService.startGame(email, game!!.id)

        setupLetterRack()
        setupLayTiles()
    }

    private fun setupLetterRack() {
        val rack = letterRackService.getLetterRackForPlayerInGame(email, game!!.id)
        rack.tiles.removeAll { true }
        rack.tiles.addAll(listOf(
                Tile(letterService.getLetterByCharAndLanguage('A', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('B', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('A', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('S', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('E', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('D', language), rack),
                Tile(letterService.getLetterByCharAndLanguage('.', language), rack)
        ))

        letterRackRepository.save(rack)
    }

    private fun setupLayTiles() {
        val tilesToPlay = listOf(
                LaidDownTileDTO('A', 5, 7),
                LaidDownTileDTO('B', 6, 7),
                LaidDownTileDTO('A', 7, 7),
                LaidDownTileDTO('S', 8, 7),
                LaidDownTileDTO('E', 9, 7)
        )

        layTiles(tilesToPlay)
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
    }

    private fun layTiles(tiles: List<LaidDownTileDTO>): ResultActions {
        val request = mapper.writeValueAsString(PlaceTilesRequestDTO(tiles))

        return mockMvc.perform(
                post("/api/games/${game?.id}/tiles")
                        .header("Authorization", "Bearer $accessToken")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request)
        )
    }

    @Test
    @Transactional
    fun getAllTurnsByGameId() {
        val allTurns = mockMvc.perform(MockMvcRequestBuilders.get("/api/games/${game?.id}/turns")
                .header("Authorization", "Bearer $accessToken"))
                .andExpect { status().isOk }
                .andReturn()
                .response.contentAsString

        println("result turn get: $allTurns")
        Assert.assertNotNull(allTurns)
    }

    @Test
    @Transactional
    fun createGame() {
        val contentAsString = mockMvc
                .perform(post("/api/games/")
                        .content(mapper.writeValueAsString(NewGameRequestDTO(gameName, language, players)))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .header("Authorization", "Bearer $accessToken"))
                .andExpect { status().isOk }
                .andReturn()
                .response.contentAsString
        println("result createGame: $contentAsString")
    }

    @Test
    @Transactional
    fun deleteGame() {
        val createdGame = mockMvc.perform(post("/api/games")
                .content(mapper.writeValueAsString(NewGameRequestDTO(gameName, language, players)))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect { status().isCreated }
                .andReturn()
                .response.contentAsString

        val id = jsonParser.parseMap(createdGame)["id"].toString()
        mockMvc.perform(delete("/api/games/$id")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .header("Authorization", "Bearer $accessToken"))
                .andExpect { status().isOk }
                .andReturn()
                .response.contentAsString
    }

    private fun getToken(email: String, password: String): String {
        val params = LinkedMultiValueMap<String, String>()
        params.add("grant_type", "password")
        params.add("client_id", "devglan-client")
        params.add("username", email)
        params.add("password", password)

        val resultString = mockMvc.perform(MockMvcRequestBuilders.post("/oauth/token", params).params(params)
                .with(SecurityMockMvcRequestPostProcessors.httpBasic("devglan-client", "devglan-secret"))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().response.contentAsString
        return jsonParser.parseMap(resultString)["access_token"].toString()
    }

    private fun register(username: String, email: String, password: String) {
        val body = mapper.writeValueAsString(SignUpForm(username, email, password))
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/register")
                .contentType(MediaType.APPLICATION_JSON).content(body))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }
}
