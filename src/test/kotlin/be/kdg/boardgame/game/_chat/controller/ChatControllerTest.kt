package be.kdg.boardgame.game._chat.controller

import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.game._chat.dto.MessageDTO
import be.kdg.boardgame.game._chat.service.MessageService
import be.kdg.boardgame.game._game.dto.NewGameRequestDTO
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.oauth2.common.util.JacksonJsonParser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.util.LinkedMultiValueMap
import javax.transaction.Transactional

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class ChatControllerTest {
    @Autowired
    lateinit var mockMvc: MockMvc
    @Autowired
    private lateinit var mapper: ObjectMapper
    @Autowired
    private lateinit var chatMessageService: MessageService

    private val email: String = "hasfriends@gmail.com"
    private val password: String = "hasfriends@gmail.com"
    private val username = "test"
    private var accessToken: String? = null
    private val gameName = "testGame"
    private val language = "en_US"
    private var gameId = ""
    val jsonParser = JacksonJsonParser()

    @Before
    fun setUp() {
        // register
        register(username, email, password)
        accessToken = getToken(email, password)

        // save mock game
        gameId = createGame(gameName, language, mutableListOf(username, "quinten", "None", "None"))

        // simulate chat history between a bot and the creator of the game
        chatMessageService.save(gameId, "hello from quinten", "quinten")
        chatMessageService.save(gameId, "hello from test", username)
        (0..3).map { n ->
            chatMessageService.save(
                    gameId,
                    "hello world $n",
                    if (Math.random() >= 0.5) username else "quinten")
        }
    }

    @Test
    @Transactional
    fun testGetChatHistoryApi() {
        val messages = mockMvc
                .perform(get("/api/chats/$gameId/messages/")
                        .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isOk)
                .andReturn().response.contentAsString
        println(messages)
    }

    @Test
    @Transactional
    fun testDifferentMessagesDifferentPlayers() {
        val messages = mockMvc
                .perform(get("/api/chats/$gameId/messages/")
                        .header("Authorization", "Bearer $accessToken"))
                .andExpect(status().isOk)
                .andReturn().response.contentAsString
        println(messages)

        val typeFactory = mapper.typeFactory
        val messagesList: List<MessageDTO> = mapper.readValue(messages, typeFactory.constructCollectionType(List::class.java, MessageDTO::class.java))
        messagesList.forEach { println(it) }

        // messages should be present
        assertTrue(messagesList.isNotEmpty())

        // test different users
        val usernames = messagesList.map { m -> m.name }
        val areAllUsersChatsFetched = usernames.containsAll(mutableListOf("quinten", username))
        assertTrue(areAllUsersChatsFetched)
    }


    private fun getToken(email: String, password: String): String {
        val params = LinkedMultiValueMap<String, String>()
        params.add("grant_type", "password")
        params.add("client_id", "devglan-client")
        params.add("username", email)
        params.add("password", password)

        val resultString = mockMvc.perform(MockMvcRequestBuilders.post("/oauth/token", params).params(params)
                .with(SecurityMockMvcRequestPostProcessors.httpBasic("devglan-client", "devglan-secret"))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().response.contentAsString
        return jsonParser.parseMap(resultString)["access_token"].toString()
    }

    private fun register(username: String, email: String, password: String) {
        val body = mapper.writeValueAsString(SignUpForm(username, email, password))
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/register")
                .contentType(MediaType.APPLICATION_JSON).content(body))
                .andExpect(status().isOk)
    }

    private fun createGame(title: String, lang: String, players: MutableList<String>): String {
//        game = gameService.createGame(email, gameName, language, mutableListOf("Bot", "None", "None"))
        val body = mapper.writeValueAsString(NewGameRequestDTO(title, lang, players))

        val createdGame = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/games")
                .header("Authorization", "Bearer $accessToken")
                .contentType(MediaType.APPLICATION_JSON).content(body))
                .andExpect(status().isOk)
                .andReturn().response.contentAsString

        println(jsonParser.parseMap(createdGame)["id"].toString())
        return jsonParser.parseMap(createdGame)["id"].toString()
    }
}
